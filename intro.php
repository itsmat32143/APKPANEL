<?php 

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);
///////////VARIABLES//////////////////
header("Cache-Control: no-cache, no-store, must-revalidate"); 
// HTTP 1.1.
header("Pragma: no-cache"); 
// HTTP 1.0.
header("Expires: 0");  
// Proxies.

//db call 
$db = new SQLite3('./api/ottrun/intro.db');
//table name
$table_name = "Vilhaointro";
//current file var
$base_file = basename($_SERVER["SCRIPT_NAME"]);
// file mp4
$intro_file = 'intro.mp4'; 
$target_dir = 'intro/';

$file_path = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] !== 'off') ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/'.$target_dir;

if (!file_exists('intro')) {
	mkdir('intro', 511, true);
}
//create if not
$db->exec("CREATE TABLE IF NOT EXISTS {$table_name}(id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, url VARCHAR(100), intro_on_off TEXT, type TEXT)");

$rows = $db->query("SELECT COUNT(*) as count FROM {$table_name}");
$row = $rows->fetchArray();
$numRows = $row['count'];
if ($numRows == 0)
{
    $introurl = $file_path.$intro_file;
	$db->exec("INSERT INTO {$table_name}(id, url, intro_on_off, type) VALUES('1', '".$introurl."', 'yes', '0')");
}

//update call
@$resU = $db->query("SELECT * FROM {$table_name} WHERE id='1'");
@$rowU=$resU->fetchArray();
$new_url = str_replace(' ', '', $rowU['url']);
$intro_on_off = $rowU['intro_on_off'];

/////////////////////////////////////////////////////////

if (isset($_POST['intro_on_off'])) {

	if (empty($_POST['intro_on_off']) || $_POST['intro_on_off'] == 'no') {
		$intro_on_off = 'no';
		
		$msg = '?messageE=Saved!, Intro video Inactive.';
	}
	else {
		$intro_on_off = $_POST['intro_on_off'];
		
		$msg = '?messageS=Saved!, Intro video Activated.';
	}
    
	$db->exec("UPDATE {$table_name} SET intro_on_off='".$intro_on_off."'
									WHERE 
										id='1'");
	$db->close();
	
	header("Location: {$base_file}" . $msg);
}


if (isset($_POST['submit'])) {
	
	if ($_POST['type'] == '0') {
	    
	$target_file = $target_dir. $intro_file;
//$intro_file = 'intro.mp4'; 
//$target_dir = 'intro/';

//echo $target_file;
//die;
	$uploadOk = 1;
	
	$file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
	$extensions = ['gif', 'GIF', 'mpeg', 'MPEG', 'mov', 'MOV', 'avi', 'AVI', 'mp4', 'MP4'];



	if (file_exists($target_file)) {
		unlink($target_file);
	$msg = '?messageA=The file already exists and has been replaced.';
	}else { 
		$msg = '?messageS=Success, MP4 File uploaded.'; }

	$uploadOk = 1;

	if (15000000 < $_FILES['image']['size']) {
		$msg = '?messageE=Sorry, your file is too large.';
		//echo $msg;
		$uploadOk = 0;
	}
	if (in_array($file_ext, $extensions) === false) {
		//$errors[] = 'extension not allowed, please choose a JPEG or PNG or GIF or MP4 file.';
				$msg = '?messageE=Sorry, only MP4 files are allowed.';
		//echo $msg;
		
	
		$uploadOk = 0;
	}


	if ($uploadOk == 0) {
	    
	header("Location: {$base_file}".$msg);
	}
	else if (move_uploaded_file($_FILES['image']['tmp_name'], $target_file)) {
		$intro_patch = '' . $file_path . $intro_file . '';
		
		$db->exec("UPDATE {$table_name} SET url='".$intro_patch."',
										intro_on_off='".$intro_on_off."',
										type='".$_POST['type']."'
									WHERE id='1'");
	$db->close();
	header("Location: {$base_file}".$msg);
	
	}
}else{
	$url = $_POST['url'];
    $target_file = $target_dir . basename($url);
    
		
	$file_ext = strtolower(end(explode('.', $url)));
	$extensions = ['jpeg', 'jpg', 'png', 'gif', 'JPEG', 'JPG', 'PNG', 'GIF', 'mp4', 'MP4'];
	
	if (in_array($file_ext, $extensions) === false) {
		//$errors[] = 'extension not allowed, please choose a JPEG or PNG or GIF or MP4 file.';
				$msg = '?messageE=Sorry, only MP4 files are allowed.';
		//echo $msg;
		
		
	header("Location: {$base_file}".$msg);
	}else{
	
		$msg = '?messageS=Saved!, Url of saved MP4 file..';
		
		$intro_patch = $url;
		
		$db->exec("UPDATE {$table_name} SET url='".$intro_patch."',
										intro_on_off='".$intro_on_off."',
										type='".$_POST['type']."'
									WHERE id='1'");
	$db->close();
	header("Location: {$base_file}".$msg);
	}
  }
}



ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

?>













<!doctype html>
<html lang="en">

<head>



    <?php include('assets/includes/head-css.php') ?>

</head>

<body data-sidebar="dark">

    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner">
                <i class="ri-loader-line spin-icon"></i>
            </div>
        </div>
    </div>

    <div id="layout-wrapper">

        <?php include('assets/includes/topbar.php') ?>

        <?php include('assets/includes/sidebar.php') ?>

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->

                    <!-- Column -->


<!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->

                            <div class="row">
                    <!-- Column -->

                    <div class="">
                        <div class="">
                                                
							<div class="">
							    <div class="card border-left-primary shadow h-100 card shadow mb-4">
                    

                <hr class="mt-0">
						<center>
						    						    <a href="index.php" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="https://i.imgur.com/QNjgua6.png" alt="" height="42">
                    </span>
                    <span class="logo-lg">
                        <img src="https://i.imgur.com/QNjgua6.png" alt="" height="40">
                    </span>
                </a>
							<h2><i class="icon icon-wrench"></i> Intro Video</h2>
						</center>
					</div>

                			</div>
                			<div class="card border-left-primary shadow h-100 card shadow mb-4">
                			    <center>
        <form name="" id="onoff" method="post" enctype="multipart/form-data">
<br><br>
                        <div class="form-group ">
                            	<h6 align="center" class="m-0 font-weight-bold text-white">
                             <strong> Active Intro Video ?</strong>
                            </h6>
<p>
                        <label>Yes <input type="radio" onchange="formSubmit(this)" class="option--input" id="radio1" name="intro_on_off" value="yes"  <?php

if ($intro_on_off == 'yes') {
	echo 'checked=\\"checked\\"';
}
?>
> </label> &nbsp&nbsp&nbsp
            <label>No <input type="radio" onchange="formSubmit(this)" class="option--input" id="radio2" name="intro_on_off" value="no" <?php

if ($intro_on_off == 'no') {
	echo 'checked=\\"checked\\"';
}
?>
></label>
                     </form>
                </div></center>
                
						<form method="post" enctype="multipart/form-data">
                           
                                <div class="form-group ">
                                   <label class="control-label " for="vpn_config">
                                       <strong>Select Intro Video File Type</strong>
                                       <br><br>
                                  </label>
                                 <div class="input-group">
									  <select class="form-control type" id="type" name="type">
										  <option data-value="op0" value="0" <?=$rowU['type']=='0'?'selected':'' ?>>Upload MP4 File
										  </option>
										  <option data-value="op1" value="1" <?=$rowU['type']=='1'?'selected':'' ?>>Enter MP4 External URL
										  </option>
									  </select>
                                    </div>
                                </div>
                        
                    <div class="activeu">
                     <div class="form-group ">
                             <label class="control-label"><strong> Enter MP4 External URL </strong></label>
                                <div class="input-group">
                                 <input  class="form-control" type="text" name="url" value="<?php echo $new_url; ?>" placeholder="Enter URL file.mp4">
                                </div>
                             </div>
                         </div>

                      <div class="actived">
                        <div class="form-group ">
                            <br>
                            <label class="control-label"> <strong> Upload Intro Video File</strong></label>
                            <br><br>
						      <div class="input-group">
						      	<div class="custom-file">
					        	<input type="file" class="custom-file-input" name="image" id="intro" placeholder="Choose Intro" onchange="uploadintro(this)" aria-describedby="intro">
			<label class="custom-file-label" for="intro" placeholder="Choose Intro"><span id="image-intro"></span></label>
				          	   </div>
			                </div>
		        	     </div>
		    	       </div>
			        
			         <div align="center" class="form-group">
					<br>
					<button class="btn btn-danger" name="submit"  type="submit"><span class="icon text-white-50"><i class="fa fa-save"></i>&nbsp;&nbsp;</span><span class="text">  Save Settings</span></button>
                                </div>

							</form>

                         <div align="center"  class="form-group">
						 
						 <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Current Intro</div>
                                              <center>
                                   <div class="card-body align-items-center">
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                                        <video controls="" autoplay="" name="media" width="60%">
                                            <source src="<?php if ($intro_on_off == 'yes') {
$intro = $new_url;
}else{
$intro = './intro/nointro.mp4';   

}
echo $intro . '?' . time(); ?>" type="video/mp4"></video>
                                    </div>                                            <br>
                                </div>
						 
                        </div>

                      
                            </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script>
//select activecode form
//var response = {};
//response.val = "op2";
//$("#codemode option[data-value='" + response.val +"']").attr("selected","selected");

//hide activecode form
$('.actived').show(); 

$('.activeu').hide(); 


//Show/hide activecode select
$(document).ready(function(){
  $('.type').change(function(){
    if($('.type').val() > 0) {
      $('.actived').hide(); 
      $('.activeu').show(); 
    } else {
      $('.activeu').hide(); 
      $('.actived').show(); 
     document.getElementById("activeu").value = ' ';
    } 
  });
  $('.type').ready(function(){
    if($('.type').val() > 0) {
      $('.actived').hide(); 
      $('.activeu').show(); 
    }else {
      $('.activeu').hide(); 
      $('.actived').show(); 
     document.getElementById("actived").value = ' ';
      
    } 
  });
});
</script>
<script>
function formSubmit(radioObj){
  if(radioObj.checked){
    document.getElementById("onoff").submit();

  }
}
</script>







<?php
// JAVA TARGET
echo '<script>';
echo 'function uploadintro(target) {' . "\n";
echo "\t" . 'document.getElementById("image-intro").innerHTML = target.files[0].name;' . "\n";
echo '}' . "\n";
echo "\n";
echo '$(document).ready(function() {' . "\n";
echo '    if (location.hash) {' . "\n";
echo '        $("a[href=\'" + location.hash + "\']").tab("show");' . "\n";
echo '    }' . "\n";
echo '    $(document.body).on("click", "a[data-toggle=\'tab\']", function(event) {' . "\n";
echo '        location.hash = this.getAttribute("href");' . "\n";
echo '    });' . "\n";
echo '});' . "\n";
echo '$(window).on("popstate", function() {' . "\n";
echo '    var anchor = location.hash || $("a[data-toggle=\'tab\']").first().attr("href");' . "\n";
echo '    $("a[href=\'" + anchor + "\']").tab("show");' . "\n";
echo '});' . "\n";
echo "\n";
echo '</script>';
//INCLUDE FUNCTIONS


  include('assets/includes/footer.php');


   include('assets/includes/right-sidebar.php');

   include('assets/includes/vendor-scripts.php');






echo '</body>' . "\n";

echo '
<style>




.input-group {
    position: relative;
    display: flex;
    text-align: center;
    /* align-items: center; */
    width: 90%;
    margin-left: auto;
    margin-right: auto;
}

body {
    margin: 0;
    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    text-align: left;
        margin-top: 95px;

}

.row {
    display: -ms-flexbox;
    display: contents;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}


.card {
    margin-left: 270px;
    margin-right: 20px;
    text-align: center;
}
</style>' . "\n";
?>



















                        
                        
                        
                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include('assets/includes/footer.php'); ?>
    </div>

    </div>

    <?php include('assets/includes/right-sidebar.php'); ?>

    <?php include('assets/includes/vendor-scripts.php'); ?>

    <script src="./assets/js/app.js"></script>

</body>

</html>