<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

session_start();
include 'assets/includes/db.php';
include 'assets/includes/config.php';

if ($_ERRORS) {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}

date_default_timezone_set('UTC');
if (!isset($_SESSION['loggedin']) && !$_SESSION['loggedin']) {
	header('location: logout.php');
}

if (isset($_POST['mnt_submit'])) {
	$sql = 'UPDATE xc_xciptv_app SET ';
	$sql .= 'mnt_message = \'' . $_POST['mnt_message'] . '\', ';
	$sql .= 'mnt_status = \'' . $_POST['mnt_status'] . '\', ';
	$sql .= 'mnt_expire = \'' . $_POST['mnt_expire'] . '\';';
	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_messages.php');
}

if (isset($_POST['ann_submit'])) {
	$sql = 'UPDATE xc_xciptv_ann SET ';
	$sql .= 'announcement = \'' . $_POST['announcement'] . '\', ';
	$sql .= 'status = \'' . $_POST['ann-status'] . '\', ';
	$sql .= 'channel = \'' . $_POST['channel'] . '\', ';
	$sql .= 'expire = \'' . $_POST['expire'] . '\', ';
	$sql .= 'interval = \'' . $_POST['interval'] . '\', ';
	$sql .= 'disappear = \'' . $_POST['disappear'] . '\' ';
	$sql .= 'WHERE id = 1;';
	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_messages.php');
}

if (isset($_POST['post_message_change'])) {
	$sql = 'REPLACE INTO ottrun_messages(';
	$sql .= 'username, ';
	$sql .= 'expiration, ';
	$sql .= 'status, ';
	$sql .= 'message) ';
	$sql .= 'VALUES (';
	$sql .= '\'' . $_POST['username'] . '\', ';
	$sql .= '\'' . $_POST['expiration'] . '\', ';
	$sql .= '\'ACTIVE\', ';
	$sql .= '\'' . $_POST['message'] . '\');';
	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_messages.php');
}

if (isset($_POST['delete_id'])) {
	$sql = 'DELETE FROM ottrun_messages ';
	$sql .= 'WHERE id = ' . $_POST['delete_id'] . ';';
	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_messages.php');
}

echo '<!doctype html>' . "\r\n" . '<html lang="en">' . "\r\n\r\n" . '<head>' . "\r\n\r\n" . '    ';
include 'assets/includes/title-meta.php';
echo "\r\n" . '    ';
include 'assets/includes/head-css.php';
echo "\r\n" . '</head>' . "\r\n\r\n" . '<body data-sidebar="dark">' . "\r\n\r\n" . '    <!-- Loader -->' . "\r\n" . '    <div id="preloader">' . "\r\n" . '        <div id="status">' . "\r\n" . '            <div class="spinner">' . "\r\n" . '                <i class="ri-loader-line spin-icon"></i>' . "\r\n" . '            </div>' . "\r\n" . '        </div>' . "\r\n" . '    </div>' . "\r\n\r\n" . '    <div id="layout-wrapper">' . "\r\n\r\n" . '        ';
include 'assets/includes/topbar.php';
echo "\r\n" . '        ';
include 'assets/includes/sidebar.php';
echo "\r\n" . '        <div class="main-content">' . "\r\n\r\n" . '            <div class="page-content">' . "\r\n" . '                <div class="container-fluid">' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="page-title-box d-flex align-items-center justify-content-between">' . "\r\n" . '                                <h4 class="mb-0"> </h4>' . "\r\n\r\n" . '                                <div class="page-title-right">' . "\r\n" . '                                    <ol class="breadcrumb m-0">' . "\r\n" . '                                        <li class="breadcrumb-item"><a href="javascript: void(0);">OTTRun XCIPTV</a></li>' . "\r\n" . '                                        <li class="breadcrumb-item active">Messages & Announcements</li>' . "\r\n" . '                                    </ol>' . "\r\n" . '                                </div>' . "\r\n\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-6 mx-auto">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <h4 class="card-title">Maintenance Message</h4>' . "\r\n" . '                                    <p class="card-title-desc">Apply Maintenence mode</br>This setting will affect all connected applications.</p>' . "\r\n\r\n" . '                                    <br />' . "\r\n\r\n" . '                                    <form method="post">' . "\r\n" . '                                        <div class="form-group">' . "\r\n" . '                                            <label for="mnt_message">Maintenance message</label>' . "\r\n" . '                                            <input class="form-control" id="mnt_message" name="mnt_message" value="';
echo $xciptv_app_data['mnt_message'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group">' . "\r\n" . '                                            <label for="mnt_status">Status</label>' . "\r\n" . '                                            <select class="form-control" id="mnt_status" name="mnt_status">' . "\r\n" . '                                                <option value="INACTIVE" ';
echo $xciptv_app_data['mnt_status'] == 'INACTIVE' ? 'selected' : '';
echo '>INACTIVE</option>' . "\r\n" . '                                                <option value="ACTIVE" ';
echo $xciptv_app_data['mnt_status'] == 'ACTIVE' ? 'selected' : '';
echo '>ACTIVE</option>' . "\r\n" . '                                            </select>' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group">' . "\r\n" . '                                            <label for="mnt_expire">End date</label>' . "\r\n" . '                                            <input type="text" class="form-control" id="mnt_expire" name="mnt_expire" value="';
echo $xciptv_app_data['mnt_expire'];
echo '" />' . "\r\n" . '                                        </div>' . "\r\n" . '                                        <div class="form-group">' . "\r\n" . '                                            <button class="btn btn-primary" name="mnt_submit" type="submit">Save</button>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </form>' . "\r\n\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n\r\n" . '                        <div class="col-6 mx-auto">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <h4 class="card-title">Announement Message</h4>' . "\r\n" . '                                    <p class="card-title-desc">Send in-app Announcement</br>This announcement will apply to all connected applicaitons.</p>' . "\r\n\r\n" . '                                    <br />' . "\r\n\r\n" . '                                    <form method="post">' . "\r\n" . '                                        <div class="form-group col-9 float-left">' . "\r\n" . '                                            <label for="announcement">Announcement</label>' . "\r\n" . '                                            <input class="form-control" id="announcement" name="announcement" value="';
echo $xciptv_ann_data['announcement'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n" . '                                        <div class="form-group col-3 float-right">' . "\r\n" . '                                            <label for="select">Status</label>' . "\r\n" . '                                            <select class="form-control" id="ann-status" name="ann-status">' . "\r\n" . '                                                <option value="INACTIVE" ';
echo $xciptv_ann_data['status'] == 'INACTIVE' ? 'selected' : '';
echo '>INACTIVE</option>' . "\r\n" . '                                                <option value="ACTIVE" ';
echo $xciptv_ann_data['status'] == 'ACTIVE' ? 'selected' : '';
echo '>ACTIVE</option>' . "\r\n" . '                                            </select>' . "\r\n" . '                                        </div>' . "\r\n" . '                                        <div class="form-group col-6 float-left">' . "\r\n" . '                                            <label for="channel">Channel (All or Channel name)</label>' . "\r\n" . '                                            <input class="form-control" id="channel" name="channel" value="';
echo $xciptv_ann_data['channel'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n" . '                                        <div class="form-group col-6 float-right">' . "\r\n" . '                                            <label for="expire">Expiration</label>' . "\r\n" . '                                            <input type="text" class="form-control" id="expire" name="expire" placeholder="YYYY-MM-DD HH:MM:SS" value="';
echo $xciptv_ann_data['expire'];
echo '">' . "\r\n" . '                                        </div>' . "\r\n" . '                                        <div class="form-group col-6 float-left">' . "\r\n" . '                                            <label for="interval">Display Interval (Mins)</label>' . "\r\n" . '                                            <input class="form-control" id="interval" name="interval" placeholder="1" value="';
echo $xciptv_ann_data['interval'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n" . '                                        <div class="form-group col-6 float-right">' . "\r\n" . '                                            <label for="disappear">Disappear (Mins)</label>' . "\r\n" . '                                            <input class="form-control" id="disappear" name="disappear" placeholder="5" value="';
echo $xciptv_ann_data['disappear'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n" . '                                        <div class="form-group">' . "\r\n" . '                                            <button class="btn btn-primary" name="ann_submit" type="submit">Save</button>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </form>' . "\r\n\r\n\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12 col-md-4">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n" . '                                    <h5 class="card-title">Add Message</h5>' . "\r\n" . '                                    <hr class="mt-0" />' . "\r\n" . '                                    <form method="POST" id="post_message_change">' . "\r\n" . '                                        <div class="form-row">' . "\r\n" . '                                            <div class="form-group col-12">' . "\r\n" . '                                                <label for="username">Username</label>' . "\r\n" . '                                                <input type="text" id="username" name="username" class="form-control" value="';
echo @$_GET['msg'];
echo '"/>' . "\r\n" . '                                            </div>' . "\r\n" . '                                            <div class="form-group col-12">' . "\r\n" . '                                                <label for="expiration">Expiration</label>' . "\r\n" . '                                                <input type="text" class="form-control" id="expiration" name="expiration" placeholder="YYYY-MM-DD HH:MM:SS">' . "\r\n" . '                                            </div>' . "\r\n" . '                                            <div class="form-group col-12">' . "\r\n" . '                                                <label for="message">Message</label>' . "\r\n" . '                                                <textarea name="message" id="message" rows="1" class="form-control"></textarea>' . "\r\n" . '                                            </div>' . "\r\n" . '                                            <hr class="my-0" />' . "\r\n" . '                                        </div>' . "\r\n" . '                                        <div class="text-center">' . "\r\n" . '                                            <button class="btn btn-primary" name="post_message_change" type="submit">' . "\r\n" . '                                                Send' . "\r\n" . '                                            </button>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </form>' . "\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                        <div class="col-12 col-md-8">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n" . '                                    <h5 class="card-title">Current Messages</h5>' . "\r\n" . '                                    <hr class="mt-0" />' . "\r\n" . '                                    <div class="table-responsive no-padding">' . "\r\n" . '                                        <table id="datatable" class="table dt-responsive table-striped table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">' . "\r\n" . '                                            <thead>' . "\r\n" . '                                                <tr>' . "\r\n" . '                                                    <th hidden>#</th>' . "\r\n" . '                                                    <th>Username</th>' . "\r\n" . '                                                    <th>Expiration</th>' . "\r\n" . '                                                    <th>Message</th>' . "\r\n" . '                                                    <th>Actions</th>' . "\r\n" . '                                                </tr>' . "\r\n" . '                                            </thead>' . "\r\n" . '                                            <tbody>' . "\r\n" . '                                                ';

while ($row = $ottrun_messages->fetchArray()) {
	echo '                                                    <tr>' . "\r\n" . '                                                        <td hidden>';
	echo $row['id'];
	echo '</td>' . "\r\n" . '                                                        <td>';
	echo $row['username'];
	echo '</td>' . "\r\n" . '                                                        <td>';
	echo $row['expiration'];
	echo '</td>' . "\r\n" . '                                                        <td>';
	echo $row['message'];
	echo '</td>' . "\r\n" . '                                                        <td>' . "\r\n" . '                                                            <button type="button" data-toggle="modal" class="edit-button btn-sm btn-primary waves-effect waves-light"><i class="dripicons-document-edit"></i></button>' . "\r\n" . '                                                            <button type="button" data-toggle="modal" data-target="#delete_modal" class="delete-button btn-sm btn-danger waves-effect waves-light"><i class="dripicons-document-delete"></i></button>' . "\r\n" . '                                                        </td>' . "\r\n" . '                                                    </tr>' . "\r\n" . '                                                ';
}

echo '                                            </tbody>' . "\r\n" . '                                        </table>' . "\r\n" . '                                    </div>' . "\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n\r\n" . '                        <div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">' . "\r\n" . '                            <div class="modal-dialog modal-dialog-centered" role="document">' . "\r\n" . '                                <div class="modal-content">' . "\r\n" . '                                    <div class="modal-header border-bottom-0">' . "\r\n" . '                                        <h5 class="modal-title" id="deleteModalLabel">Delete Changelog</h5>' . "\r\n" . '                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">' . "\r\n" . '                                            <span class="text-danger" aria-hidden="true">&times;</span>' . "\r\n" . '                                        </button>' . "\r\n" . '                                    </div>' . "\r\n" . '                                    <form method="POST">' . "\r\n" . '                                        <div class="modal-body">' . "\r\n" . '                                            <div class="form-group" hidden>' . "\r\n" . '                                                <input type="text" class="form-control" id="delete_id" name="delete_id">' . "\r\n" . '                                            </div>' . "\r\n" . '                                            <h3 class="text-center mt-4">Are you sure?</h3>' . "\r\n" . '                                        </div>' . "\r\n" . '                                        <div class="modal-footer text-center mt-4">' . "\r\n" . '                                            <button type="submit" name="delete_dns" class="btn-sm btn-danger">Delete</button>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </form>' . "\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n\r\n" . '                        <script>' . "\r\n" . '                            $(function() {' . "\r\n" . '                                $(".edit-button").on(\'click\', function() {' . "\r\n" . '                                    var currentRow = $(this).closest("tr");' . "\r\n" . '                                    var id = currentRow.find("td:eq(0)").text();' . "\r\n" . '                                    var username = currentRow.find("td:eq(1)").text();' . "\r\n" . '                                    var expiration = currentRow.find("td:eq(2)").text();' . "\r\n" . '                                    var message = currentRow.find("td:eq(3)").text();' . "\r\n" . '                                    $("#id").val(id);' . "\r\n" . '                                    $("#username").val(username);' . "\r\n" . '                                    $("#expiration").val(expiration);' . "\r\n" . '                                    $("#message").val(message);' . "\r\n" . '                                });' . "\r\n" . '                                $(".delete-button").on(\'click\', function() {' . "\r\n" . '                                    var currentRow = $(this).closest("tr");' . "\r\n" . '                                    var id = currentRow.find("td:eq(0)").text();' . "\r\n" . '                                    $("#delete_id").val(id);' . "\r\n" . '                                });' . "\r\n" . '                            });' . "\r\n" . '                        </script>' . "\r\n\r\n" . '                    </div>' . "\r\n" . '                </div>' . "\r\n\r\n" . '                ';
include 'assets/includes/footer.php';
echo '            </div>' . "\r\n\r\n" . '        </div>' . "\r\n\r\n" . '        ';
include 'assets/includes/right-sidebar.php';
echo "\r\n" . '        ';
include 'assets/includes/vendor-scripts.php';
echo "\r\n" . '        <script src="./assets/js/app.js"></script>' . "\r\n\r\n" . '</body>' . "\r\n\r\n" . '</html>';

?>