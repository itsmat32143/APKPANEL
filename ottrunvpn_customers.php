<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

session_start();
include 'assets/includes/db.php';
include 'assets/includes/config.php';

if ($_ERRORS) {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}

date_default_timezone_set('UTC');
if (!isset($_SESSION['loggedin']) && !$_SESSION['loggedin']) {
	header('location: logout.php');
}

if (isset($_POST['add_customer'])) {
	$sql = 'INSERT INTO extra_customers_ottrun (';
	$sql .= 'username, ';
	$sql .= 'password, ';
	$sql .= 'status, ';
	$sql .= 'expiry) ';
	$sql .= 'VALUES (';
	$sql .= '\'' . $_POST['add-username'] . '\', ';
	$sql .= '\'' . $_POST['add-password'] . '\', ';
	$sql .= '\'Active\',';
	$sql .= '\'' . $_POST['add-expiry'] . '\');';
	$sqlite3->exec($sql);
	header('Location: ottrunvpn_customers.php');
}

if (isset($_POST['edit_customer'])) {
	$sql = 'UPDATE extra_customers_ottrun SET ';
	$sql .= 'username = \'' . $_POST['edit-username'] . '\', ';
	$sql .= 'password = \'' . $_POST['edit-password'] . '\', ';
	$sql .= 'status = \'' . $_POST['edit-status'] . '\',';
	$sql .= 'expiry = \'' . $_POST['edit-expiry'] . '\' ';
	$sql .= 'WHERE id = ' . $_POST['edit-id'] . ';';
	$sqlite3->exec($sql);
	header('Location: ottrunvpn_customers.php');
}

if (isset($_GET['disable_customer'])) {
	$sql = 'UPDATE extra_customers_ottrun SET ';
	$sql .= 'status = \'Disabled\' ';
	$sql .= 'WHERE id = ' . $_GET['disable_customer'] . ';';
	$sqlite3->exec($sql);
	header('Location: ottrunvpn_customers.php');
}

if (isset($_GET['enable_customer'])) {
	$sql = 'UPDATE extra_customers_ottrun SET ';
	$sql .= 'status = \'Active\' ';
	$sql .= 'WHERE id = ' . $_GET['enable_customer'] . ';';
	$sqlite3->exec($sql);
	header('Location: ottrunvpn_customers.php');
}

if (isset($_POST['delete_customer'])) {
	$sql = 'DELETE FROM extra_customers_ottrun ';
	$sql .= 'WHERE id = ' . $_POST['delete-id'] . ';';
	$sqlite3->exec($sql);
	header('Location: ottrunvpn_customers.php');
}

echo '<!doctype html>' . "\r\n" . '<html lang="en">' . "\r\n\r\n" . '<head>' . "\r\n\r\n" . '    ';
include 'assets/includes/title-meta.php';
echo "\r\n" . '    ';
include 'assets/includes/head-css.php';
echo "\r\n" . '</head>' . "\r\n\r\n" . '<body data-sidebar="dark">' . "\r\n\r\n" . '    <!-- Loader -->' . "\r\n" . '    <div id="preloader">' . "\r\n" . '        <div id="status">' . "\r\n" . '            <div class="spinner">' . "\r\n" . '                <i class="ri-loader-line spin-icon"></i>' . "\r\n" . '            </div>' . "\r\n" . '        </div>' . "\r\n" . '    </div>' . "\r\n\r\n" . '    <div id="layout-wrapper">' . "\r\n\r\n" . '        ';
include 'assets/includes/topbar.php';
echo "\r\n" . '        ';
include 'assets/includes/sidebar.php';
echo "\r\n" . '        <div class="main-content">' . "\r\n\r\n" . '            <div class="page-content">' . "\r\n" . '                <div class="container-fluid">' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="page-title-box d-flex align-items-center justify-content-between">' . "\r\n" . '                                <h4 class="mb-0">OTTRun VPN</h4>' . "\r\n\r\n" . '                                <div class="page-title-right">' . "\r\n" . '                                    <ol class="breadcrumb m-0">' . "\r\n" . '                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Applications</a></li>' . "\r\n" . '                                        <li class="breadcrumb-item"><a href="javascript: void(0);">OTTRun VPN</a></li>' . "\r\n" . '                                        <li class="breadcrumb-item active">Extra Customers</li>' . "\r\n" . '                                    </ol>' . "\r\n" . '                                </div>' . "\r\n\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <h4 class="card-title">Extra Customer List</h4>' . "\r\n" . '                                    <p class="card-title-desc">' . "\r\n" . '                                        <a data-toggle="modal" data-target="#add-modal" class="add-button btn-sm btn-secondary float-right"><i class="dripicons-document-new"></i> Add New Customer</a>' . "\r\n" . '                                    </p>' . "\r\n\r\n" . '                                    <br />' . "\r\n\r\n" . '                                    <div class="table-responsive">' . "\r\n" . '                                        <table id="datatable-buttons" class="table table-striped table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">' . "\r\n" . '                                            <thead>' . "\r\n" . '                                                <tr>' . "\r\n" . '                                                    <th>ID</th>' . "\r\n" . '                                                    <th>Username</th>' . "\r\n" . '                                                    <th>Password</th>' . "\r\n" . '                                                    <th>Status</th>' . "\r\n" . '                                                    <th>Expiration Date</th>' . "\r\n" . '                                                    <th></th>' . "\r\n" . '                                                </tr>' . "\r\n" . '                                            </thead>' . "\r\n" . '                                            ';

while ($row = $customer_ottrun_data->fetchArray()) {
	echo '                                                <tr ';

	if ($expired = strtotime($row['expiry']) < time()) {
		echo 'class="table-warning"';
	}
	else if ($row['status'] == 'Disabled') {
		echo 'class="table-danger"';
	}
	else {
		echo '';
	}

	echo '>' . "\r\n" . '                                                    <td>';
	echo $row['id'];
	echo '</td>' . "\r\n" . '                                                    <td>';
	echo $row['username'];
	echo '</td>' . "\r\n" . '                                                    <td>';
	echo $row['password'];
	echo '</td>' . "\r\n" . '                                                    <td>' . "\r\n" . '                                                        <div class="badge badge-soft-';

	if ($row['status'] != 'Active') {
		echo 'danger';
	}
	else {
		echo 'success';
	}

	echo ' font-size-12">';
	echo $row['status'];
	echo '</div>' . "\r\n" . '                                                    </td>' . "\r\n" . '                                                    <td>';

	if ($expired = strtotime($row['expiry']) < time()) {
		echo '<span class="text-danger">Expired</span>';
	}
	else {
		echo $row['expiry'];
	}

	echo '</td>' . "\r\n" . '                                                    <td>' . "\r\n" . '                                                        <button type="button" data-toggle="modal" data-target="#edit-modal" class="edit-button btn-sm btn-primary waves-effect waves-light"><i class="dripicons-document-edit"></i></button>' . "\r\n" . '                                                        <button type="button" data-toggle="modal" data-target="#delete-modal" class="delete-button btn-sm btn-danger waves-effect waves-light"><i class="dripicons-trash"></i></button>' . "\r\n" . '                                                        ';

	if ($row['status'] == 'Disabled') {
		echo '                                                            <button onclick="window.location.href=\'./ottrunvpn_customers.php?enable_customer=';
		echo $row['id'];
		echo '\'" class="enable-button btn-sm btn-success waves-effect waves-light"><i class="dripicons-checkmark"></i></a>' . "\r\n" . '                                                            ';
	}
	else {
		echo '                                                                <button onclick="window.location.href=\'./ottrunvpn_customers.php?disable_customer=';
		echo $row['id'];
		echo '\'" class="disable-button btn-sm btn-danger waves-effect waves-light"><i class="dripicons-cross"></i></a>' . "\r\n" . '                                                                ';
	}

	echo '                                                    </td>' . "\r\n" . '                                                </tr>' . "\r\n" . '                                            ';
}

echo '                                        </table>' . "\r\n" . '                                    </div>' . "\r\n\r\n" . '                                    <div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">' . "\r\n" . '                                        <div class="modal-dialog modal-dialog-centered" role="document">' . "\r\n" . '                                            <div class="modal-content">' . "\r\n" . '                                                <div class="modal-header border-bottom-0">' . "\r\n" . '                                                    <h5 class="modal-title" id="addModalLabel">Add Customer</h5>' . "\r\n" . '                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">' . "\r\n" . '                                                        <span class="text-danger" aria-hidden="true">&times;</span>' . "\r\n" . '                                                    </button>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <form method="POST">' . "\r\n" . '                                                    <div class="modal-body">' . "\r\n" . '                                                        <div class="form-group">' . "\r\n" . '                                                            <label for="add-username">Userame</label>' . "\r\n" . '                                                            <input type="text" class="form-control" id="add-username" name="add-username" placeholder="Enter username">' . "\r\n" . '                                                            <small id="add-username-small" class="form-text text-muted"><em>Username for' . "\r\n" . '                                                                    login.</em></small>' . "\r\n" . '                                                        </div>' . "\r\n" . '                                                        <div class="form-group">' . "\r\n" . '                                                            <label for="add-password">Password</label>' . "\r\n" . '                                                            <input type="text" class="form-control" id="add-password" name="add-password" placeholder="Enter password">' . "\r\n" . '                                                            <small id="add-password-small" class="form-text text-muted"><em>Password for login.</em></small>' . "\r\n" . '                                                        </div>' . "\r\n" . '                                                        <div class="form-group">' . "\r\n" . '                                                            <label for="add-expiry">Expiry</label>' . "\r\n" . '                                                            <input type="date" class="form-control" id="add-expiry" name="add-expiry" value="';
echo date('Y-m-d');
echo '">' . "\r\n" . '                                                            <small id="add-expiry-small" class="form-text text-muted"><em>Expiry of customer.</em></small>' . "\r\n" . '                                                        </div>' . "\r\n" . '                                                        <div class="form-group">' . "\r\n" . '                                                            <label for="add-status">Status</label>' . "\r\n" . '                                                            <select class="custom-select" id="add-status" name="add-status">' . "\r\n" . '                                                                <option selected="Active">Active</option>' . "\r\n" . '                                                                <option value="Disabled">Disabled</option>' . "\r\n" . '                                                            </select>' . "\r\n" . '                                                            <small id="add-status-small" class="form-text text-muted"><em>Status of customer.</em></small>' . "\r\n" . '                                                        </div>' . "\r\n" . '                                                    </div>' . "\r\n" . '                                                    <div class="modal-footer text-center mt-4">' . "\r\n" . '                                                        <button type="submit" name="add_customer" class="btn-sm btn-primary">Submit</button>' . "\r\n" . '                                                    </div>' . "\r\n" . '                                                </form>' . "\r\n" . '                                            </div>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </div>' . "\r\n\r\n" . '                                    <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">' . "\r\n" . '                                        <div class="modal-dialog modal-dialog-centered" role="document">' . "\r\n" . '                                            <div class="modal-content">' . "\r\n" . '                                                <div class="modal-header border-bottom-0">' . "\r\n" . '                                                    <h5 class="modal-title" id="editModalLabel">Edit Customer</h5>' . "\r\n" . '                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">' . "\r\n" . '                                                        <span class="text-danger" aria-hidden="true">&times;</span>' . "\r\n" . '                                                    </button>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <form method="POST">' . "\r\n" . '                                                    <div class="modal-body">' . "\r\n" . '                                                        <div class="input-group row" hidden>' . "\r\n" . '                                                            <input type="text" class="form-control" id="edit-id" name="edit-id">' . "\r\n" . '                                                        </div>' . "\r\n" . '                                                        <div class="form-group">' . "\r\n" . '                                                            <label for="edit-username">Username</label>' . "\r\n" . '                                                            <input type="text" class="form-control" id="edit-username" name="edit-username" placeholder="Enter username">' . "\r\n" . '                                                            <small id="edit-username-small" class="form-text text-muted"><em>Username for' . "\r\n" . '                                                                    login.</em></small>' . "\r\n" . '                                                        </div>' . "\r\n" . '                                                        <div class="form-group">' . "\r\n" . '                                                            <label for="edit-password">Password</label>' . "\r\n" . '                                                            <input type="text" class="form-control" id="edit-password" name="edit-password" placeholder="Enter password">' . "\r\n" . '                                                            <small id="edit-password-small" class="form-text text-muted"><em>Password for login.</em></small>' . "\r\n" . '                                                        </div>' . "\r\n" . '                                                        <div class="form-group">' . "\r\n" . '                                                            <label for="edit-expiry">Expiry</label>' . "\r\n" . '                                                            <input type="date" class="form-control" id="edit-expiry" name="edit-expiry" placeholder="Enter expiry">' . "\r\n" . '                                                            <small id="edit-expiry-small" class="form-text text-muted"><em>Expiry of customer.</em></small>' . "\r\n" . '                                                        </div>' . "\r\n" . '                                                        <div class="form-group">' . "\r\n" . '                                                            <label for="edit-status">Status</label>' . "\r\n" . '                                                            <select class="custom-select" id="edit-status" name="edit-status">' . "\r\n" . '                                                                <option selected="Active">Active</option>' . "\r\n" . '                                                                <option value="Disabled">Disabled</option>' . "\r\n" . '                                                            </select>' . "\r\n" . '                                                            <small id="edit-status-small" class="form-text text-muted"><em>Status of customer.</em></small>' . "\r\n" . '                                                        </div>' . "\r\n" . '                                                    </div>' . "\r\n" . '                                                    <div class="modal-footer text-center mt-4">' . "\r\n" . '                                                        <button type="submit" name="edit_customer" class="btn-sm btn-primary">Save</button>' . "\r\n" . '                                                    </div>' . "\r\n" . '                                                </form>' . "\r\n" . '                                            </div>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </div>' . "\r\n\r\n" . '                                    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">' . "\r\n" . '                                        <div class="modal-dialog modal-dialog-centered" role="document">' . "\r\n" . '                                            <div class="modal-content">' . "\r\n" . '                                                <div class="modal-header border-bottom-0">' . "\r\n" . '                                                    <h5 class="modal-title" id="deleteModalLabel">Delete Customer</h5>' . "\r\n" . '                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">' . "\r\n" . '                                                        <span class="text-danger" aria-hidden="true">&times;</span>' . "\r\n" . '                                                    </button>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <form method="POST">' . "\r\n" . '                                                    <div class="modal-body">' . "\r\n" . '                                                        <div class="form-group" hidden>' . "\r\n" . '                                                            <input type="text" class="form-control" id="delete-id" name="delete-id">' . "\r\n" . '                                                        </div>' . "\r\n" . '                                                        <h3 class="text-center mt-4">Are you sure?</h3>' . "\r\n" . '                                                    </div>' . "\r\n" . '                                                    <div class="modal-footer text-center mt-4">' . "\r\n" . '                                                        <button type="submit" name="delete_customer" class="btn-sm btn-danger">Delete</button>' . "\r\n" . '                                                    </div>' . "\r\n" . '                                                </form>' . "\r\n" . '                                            </div>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </div>' . "\r\n\r\n" . '                                    <script>' . "\r\n" . '                                        $(function() {' . "\r\n" . '                                            $(".edit-button").on(\'click\', function() {' . "\r\n" . '                                                var currentRow = $(this).closest("tr");' . "\r\n" . '                                                var id = currentRow.find("td:eq(0)").text();' . "\r\n" . '                                                var username = currentRow.find("td:eq(1)").text();' . "\r\n" . '                                                var password = currentRow.find("td:eq(2)").text();' . "\r\n" . '                                                var status = currentRow.find(".badge").text();' . "\r\n" . '                                                var expiry = currentRow.find("td:eq(4)").text();' . "\r\n" . '                                                $("#edit-id").val(id);' . "\r\n" . '                                                $("#edit-username").val(username);' . "\r\n" . '                                                $("#edit-password").val(password);' . "\r\n" . '                                                $("#edit-status").val(status);' . "\r\n" . '                                                $("#edit-expiry").val(expiry);' . "\r\n" . '                                            });' . "\r\n" . '                                            $(".enable-button").on(\'click\', function() {' . "\r\n" . '                                                var currentRow = $(this).closest("tr");' . "\r\n" . '                                                var id = currentRow.find("td:eq(0)").text();' . "\r\n" . '                                                $("#enable-id").val(id);' . "\r\n" . '                                            });' . "\r\n" . '                                            $(".disable-button").on(\'click\', function() {' . "\r\n" . '                                                var currentRow = $(this).closest("tr");' . "\r\n" . '                                                var id = currentRow.find("td:eq(0)").text();' . "\r\n" . '                                                $("#disable-id").val(id);' . "\r\n" . '                                            });' . "\r\n" . '                                            $(".delete-button").on(\'click\', function() {' . "\r\n" . '                                                var currentRow = $(this).closest("tr");' . "\r\n" . '                                                var id = currentRow.find("td:eq(0)").text();' . "\r\n" . '                                                $("#delete-id").val(id);' . "\r\n" . '                                            });' . "\r\n" . '                                        });' . "\r\n" . '                                    </script>' . "\r\n\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n" . '                </div>' . "\r\n" . '            </div>' . "\r\n\r\n" . '            ';
include 'assets/includes/footer.php';
echo '        </div>' . "\r\n\r\n" . '    </div>' . "\r\n\r\n" . '    ';
include 'assets/includes/right-sidebar.php';
echo "\r\n" . '    ';
include 'assets/includes/vendor-scripts.php';
echo "\r\n" . '    <script src="./assets/js/app.js"></script>' . "\r\n\r\n" . '</body>' . "\r\n\r\n" . '</html>';

?>