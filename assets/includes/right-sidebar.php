<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

echo '<div class="right-bar">' . "\r\n" . '    <div data-simplebar class="h-100">' . "\r\n" . '        <div class="rightbar-title px-3 py-4">' . "\r\n" . '            <a href="javascript:void(0);" class="right-bar-toggle float-right">' . "\r\n" . '                <i class="mdi mdi-close noti-icon"></i>' . "\r\n" . '            </a>' . "\r\n" . '            <h5 class="m-0">Theme Settings</h5>' . "\r\n" . '        </div>' . "\r\n\r\n" . '        <hr class="mt-0" />' . "\r\n" . '        <h6 class="text-center mb-0">Choose Layouts</h6>' . "\r\n\r\n" . '        <div class="p-4">' . "\r\n" . '            <div class="mb-2">' . "\r\n" . '                <img src="assets/images/layouts/layout-2.jpg" class="img-fluid img-thumbnail" alt="">' . "\r\n" . '            </div>' . "\r\n" . '            <div class="custom-control custom-switch mb-3">' . "\r\n" . '                <input type="checkbox" class="custom-control-input theme-choice" id="dark-mode-switch" checked />' . "\r\n" . '                <label class="custom-control-label" for="dark-mode-switch">Dark Mode</label>' . "\r\n" . '            </div>' . "\r\n\r\n" . '            <div class="mb-2">' . "\r\n" . '                <img src="assets/images/layouts/layout-1.jpg" class="img-fluid img-thumbnail" alt="">' . "\r\n" . '            </div>' . "\r\n" . '            <div class="custom-control custom-switch mb-3">' . "\r\n" . '                <input type="checkbox" class="custom-control-input theme-choice" id="light-mode-switch" data-bsStyle="assets/css/bootstrap.min.css" data-appStyle="assets/css/app.min.css" />' . "\r\n" . '                <label class="custom-control-label" for="light-mode-switch">Light Mode</label>' . "\r\n" . '            </div>' . "\r\n" . '        </div>' . "\r\n\r\n" . '    </div>' . "\r\n" . '</div>' . "\r\n\r\n" . '<div class="rightbar-overlay"></div>';

?>