<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

echo '<script src="./assets/libs/jquery/jquery.min.js"></script>' . "\r\n" . '<script src="./assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>' . "\r\n\r\n" . '<link href="./assets/css/bootstrap-dark.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />' . "\r\n" . '<link href="./assets/css/icons.min.css" rel="stylesheet" type="text/css" />' . "\r\n" . '<link href="./assets/css/app-dark.min.css" id="app-style" rel="stylesheet" type="text/css" />' . "\r\n" . '<link href="./assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />' . "\r\n" . '<link href="./assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />' . "\r\n" . '<link href="./assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />' . "\r\n" . '<link href="./assets/libs/twitter-bootstrap-wizard/prettify.css" rel="stylesheet"  type="text/css" />';

?>