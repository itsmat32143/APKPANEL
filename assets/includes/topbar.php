<?php
include('assets/includes/config.php');
include('assets/includes/db.php');
?>
<header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">

            <div class="navbar-brand-box">
                <a href="index.php" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="https://i.imgur.com/QNjgua6.png>" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="https://i.imgur.com/QNjgua6.png" alt="" height="20">
                    </span>
                </a>

                <a href="index.php" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="https://i.imgur.com/QNjgua6.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="https://i.imgur.com/QNjgua6.png" alt="" height="20">
                    </span>
                </a>

            </div>

            <button type="button" class="btn btn-sm px-3 font-size-24 header-item waves-effect" id="vertical-menu-btn">
                <i class="ri-menu-2-line align-middle"></i>
            </button>


        </div>

        <div class="d-flex">

            <div class="dropdown d-none d-lg-inline-block ml-1">
                <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                    <i class="ri-fullscreen-line"></i>
                </button>
            </div>

            <div class="dropdown d-inline-block user-dropdown">
                <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="rounded-circle header-profile-user" src="https://i.imgur.com/gSwt73j.png" alt="Header Avatar">


                </button>
            </div>

        </div>
    </div>
</header>

<style>
a:hover {
    color: #ff0000;
    text-decoration: underline;
}


a {
    color: #c60909;
    text-decoration: none;
    background-color: transparent;
}
    
    .header-profile-user {
    height: 60px;
    width: 60px;
    background-color: #2d3448;
}
</style>