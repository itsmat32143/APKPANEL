<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

$sqlite3 = new SQLite3('./api/.cockpit-0001.db');
$sql = 'CREATE TABLE IF NOT EXISTS profile(';
$sql .= 'id INTEGER PRIMARY KEY, ';
$sql .= 'profile_name TEXT, ';
$sql .= 'username TEXT, ';
$sql .= 'password TEXT, ';
$sql .= 'avatar_url TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS panel(';
$sql .= 'title TEXT, ';
$sql .= 'logo_light TEXT, ';
$sql .= 'logo_dark TEXT, ';
$sql .= 'logo_light_sm TEXT, ';
$sql .= 'logo_dark_sm TEXT, ';
$sql .= 'login_gif TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS snoop_logs(';
$sql .= 'id INTEGER PRIMARY KEY, ';
$sql .= 'ip TEXT, ';
$sql .= 'date TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS tvsg_settings(';
$sql .= 'widget_id TEXT, ';
$sql .= 'border_color TEXT, ';
$sql .= 'background_color TEXT, ';
$sql .= 'text_color TEXT, ';
$sql .= 'auto_scroll TEXT, ';
$sql .= 'heading TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS xc_domains_ottrun(';
$sql .= 'id INTEGER PRIMARY KEY, ';
$sql .= 'name TEXT, ';
$sql .= 'ssl TEXT, ';
$sql .= 'dns TEXT, ';
$sql .= 'port TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS extra_customers_ottrun(';
$sql .= 'id INTEGER PRIMARY KEY, ';
$sql .= 'username TEXT, ';
$sql .= 'password TEXT, ';
$sql .= 'status TEXT, ';
$sql .= 'expiry TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS ovpn_credentials(';
$sql .= 'ovpn_username TEXT, ';
$sql .= 'ovpn_password TEXT, ';
$sql .= 'surfshark_username TEXT, ';
$sql .= 'surfshark_password TEXT, ';
$sql .= 'ipvanish_username TEXT, ';
$sql .= 'ipvanish_password TEXT, ';
$sql .= 'nordvpn_username TEXT, ';
$sql .= 'nordvpn_password TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS ovpn_config(';
$sql .= 'id INTEGER PRIMARY KEY, ';
$sql .= 'vpn_type TEXT, ';
$sql .= 'vpn_country TEXT, ';
$sql .= 'vpn_location TEXT, ';
$sql .= 'vpn_config TEXT, ';
$sql .= 'vpn_status TEXT, ';
$sql .= 'auth_type TEXT, ';
$sql .= 'auth_embedded TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS xc_xciptv_ann(';
$sql .= 'id INTEGER PRIMARY KEY, ';
$sql .= 'announcement TEXT, ';
$sql .= 'status TEXT, ';
$sql .= 'channel TEXT, ';
$sql .= 'expire TEXT, ';
$sql .= 'interval TEXT, ';
$sql .= 'disappear TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS xc_xciptv_ads(';
$sql .= 'admob_enabled TEXT, ';
$sql .= 'freestar_enabled TEXT, ';
$sql .= 'prebid_enabled TEXT, ';
$sql .= 'prebid_banner TEXT, ';
$sql .= 'prebid_host TEXT, ';
$sql .= 'prebid_adunitid TEXT, ';
$sql .= 'prebid_accountid TEXT, ';
$sql .= 'vast_enabled TEXT, ';
$sql .= 'vast_midrollinterval TEXT, ';
$sql .= 'vast_postrollstartat TEXT, ';
$sql .= 'vast_vodmidrollinterval TEXT, ';
$sql .= 'vast_vodprerollurl TEXT, ';
$sql .= 'vast_vodmidrollurl TEXT, ';
$sql .= 'vast_vodpostrollurl TEXT, ';
$sql .= 'vast_seriesmidrollinterval TEXT, ';
$sql .= 'vast_seriesprerollurl TEXT, ';
$sql .= 'vast_seriesmidrollurl TEXT, ';
$sql .= 'vast_seriespostrollurl TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS xc_xciptv_lic(';
$sql .= 'cid TEXT, ';
$sql .= 'appname TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS xc_xciptv_connv2(';
$sql .= 'id  INTEGER PRIMARY KEY AUTOINCREMENT, ';
$sql .= 'appid TEXT, ';
$sql .= 'version TEXT, ';
$sql .= 'device_type TEXT, ';
$sql .= 'p TEXT, ';
$sql .= 'an TEXT, ';
$sql .= 'customerid TEXT, ';
$sql .= 'userid TEXT UNIQUE, ';
$sql .= 'online TEXT, ';
$sql .= 'did TEXT, ';
$sql .= 'datetime TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS xc_ort_settings(';
$sql .= 'vlc_hw TEXT, ';
$sql .= 'last_volume_vlc TEXT, ';
$sql .= 'player_vlc_buffer TEXT, ';
$sql .= 'video_resize_vlc TEXT, ';
$sql .= 'video_subtitles_vlc TEXT, ';
$sql .= 'exo_hw TEXT, ';
$sql .= 'last_volume_exo TEXT, ';
$sql .= 'player_exo_buffer TEXT, ';
$sql .= 'video_resize_exo TEXT, ';
$sql .= 'video_subtitles_exo TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS xc_xciptv_app(';
$sql .= 'id TEXT, ';
$sql .= 'appname TEXT, ';
$sql .= 'customerid TEXT, ';
$sql .= 'expire TEXT, ';
$sql .= 'version_code TEXT, ';
$sql .= 'login_type TEXT, ';
$sql .= 'portal TEXT, ';
$sql .= 'portal2 TEXT, ';
$sql .= 'portal3 TEXT, ';
$sql .= 'portal4 TEXT, ';
$sql .= 'portal5 TEXT, ';
$sql .= 'portal_name TEXT, ';
$sql .= 'portal2_name TEXT, ';
$sql .= 'portal3_name TEXT, ';
$sql .= 'portal4_name TEXT, ';
$sql .= 'portal5_name TEXT, ';
$sql .= 'portal_vod TEXT, ';
$sql .= 'portal_series TEXT, ';
$sql .= 'apkurl TEXT, ';
$sql .= 'backupurl TEXT, ';
$sql .= 'logurl TEXT, ';
$sql .= 'apkautoupdate TEXT, ';
$sql .= 'support_email TEXT, ';
$sql .= 'support_phone TEXT, ';
$sql .= 'status TEXT, ';
$sql .= 'filter_status TEXT, ';
$sql .= 'epg_mode TEXT, ';
$sql .= 'btn_live TEXT, ';
$sql .= 'btn_live2 TEXT, ';
$sql .= 'btn_live3 TEXT, ';
$sql .= 'btn_live4 TEXT, ';
$sql .= 'btn_live5 TEXT, ';
$sql .= 'btn_vod TEXT, ';
$sql .= 'btn_vod2 TEXT, ';
$sql .= 'btn_vod3 TEXT, ';
$sql .= 'btn_vod4 TEXT, ';
$sql .= 'btn_vod5 TEXT, ';
$sql .= 'btn_epg TEXT, ';
$sql .= 'btn_epg2 TEXT, ';
$sql .= 'btn_epg3 TEXT, ';
$sql .= 'btn_epg4 TEXT, ';
$sql .= 'btn_epg5 TEXT, ';
$sql .= 'btn_series TEXT, ';
$sql .= 'btn_series2 TEXT, ';
$sql .= 'btn_series3 TEXT, ';
$sql .= 'btn_series4 TEXT, ';
$sql .= 'btn_series5 TEXT, ';
$sql .= 'btn_radio TEXT, ';
$sql .= 'btn_radio2 TEXT, ';
$sql .= 'btn_radio3 TEXT, ';
$sql .= 'btn_radio4 TEXT, ';
$sql .= 'btn_radio5 TEXT, ';
$sql .= 'btn_catchup TEXT, ';
$sql .= 'btn_catchup2 TEXT, ';
$sql .= 'btn_catchup3 TEXT, ';
$sql .= 'btn_catchup4 TEXT, ';
$sql .= 'btn_catchup5 TEXT, ';
$sql .= 'btn_account TEXT, ';
$sql .= 'btn_account2 TEXT, ';
$sql .= 'btn_account3 TEXT, ';
$sql .= 'btn_account4 TEXT, ';
$sql .= 'btn_account5 TEXT, ';
$sql .= 'btn_pr TEXT, ';
$sql .= 'btn_rec TEXT, ';
$sql .= 'btn_vpn TEXT, ';
$sql .= 'btn_noti TEXT, ';
$sql .= 'btn_update TEXT, ';
$sql .= 'btn_login_setting TEXT, ';
$sql .= 'btn_login_account TEXT, ';
$sql .= 'show_expire TEXT, ';
$sql .= 'agent TEXT, ';
$sql .= 'all_cat TEXT, ';
$sql .= 'stream_type TEXT, ';
$sql .= 'player TEXT, ';
$sql .= 'player_tv TEXT, ';
$sql .= 'player_vod TEXT, ';
$sql .= 'player_series TEXT, ';
$sql .= 'player_catchup TEXT, ';
$sql .= 'message_enable TEXT, ';
$sql .= 'announcement_enabled TEXT, ';
$sql .= 'updateuserinfo_enabled TEXT, ';
$sql .= 'whatsupcheck_enabled TEXT, ';
$sql .= 'login_logo TEXT, ';
$sql .= 'ms TEXT, ';
$sql .= 'ms2 TEXT, ';
$sql .= 'ms3 TEXT, ';
$sql .= 'btn_fav TEXT, ';
$sql .= 'btn_fav2 TEXT, ';
$sql .= 'btn_fav3 TEXT, ';
$sql .= 'btn_signup TEXT, ';
$sql .= 'bjob TEXT, ';
$sql .= 'settings_app TEXT, ';
$sql .= 'settings_account TEXT, ';
$sql .= 'logs TEXT, ';
$sql .= 'panel TEXT, ';
$sql .= 'epg_url TEXT, ';
$sql .= 'ovpn_url TEXT, ';
$sql .= 'app_language TEXT, ';
$sql .= 'load_last_channel TEXT, ';
$sql .= 'admob_banner_id TEXT, ';
$sql .= 'admob_interstitial_id TEXT, ';
$sql .= 'show_cat_count TEXT, ';
$sql .= 'activation_url TEXT, ';
$sql .= 'send_udid TEXT, ';
$sql .= 'ort_settings TEXT, ';
$sql .= 'vastconfig TEXT, ';
$sql .= 'admobconfig TEXT, ';
$sql .= 'mnt_message TEXT, ';
$sql .= 'mnt_status TEXT, ';
$sql .= 'mnt_expire TEXT, ';
$sql .= 'theme TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS ottrun_messages(';
$sql .= 'id INTEGER PRIMARY KEY AUTOINCREMENT, ';
$sql .= 'username TEXT UNIQUE, ';
$sql .= 'expiration TEXT, ';
$sql .= 'status TEXT, ';
$sql .= 'message TEXT);';
$sqlite3->exec($sql);
$sql = 'CREATE TABLE IF NOT EXISTS ovpn_sorting(';
$sql .= 'group_by TEXT, ';
$sql .= 'order_by TEXT);';
$sqlite3->exec($sql);
$result = $sqlite3->query('SELECT COUNT(*) AS count FROM profile');
$row = $result->fetchArray();
$row_count = $row['count'];

if ($row_count == 0) {
	$sql = 'INSERT INTO profile(';
	$sql .= 'id, ';
	$sql .= 'profile_name, ';
	$sql .= 'username, ';
	$sql .= 'password, ';
	$sql .= 'avatar_url) ';
	$sql .= 'VALUES(';
	$sql .= '1, ';
	$sql .= '"Admin", ';
	$sql .= '"admin", ';
	$sql .= '"admin", ';
	$sql .= '"https://i.imgur.com/0r65LVT.jpg");';
	$sqlite3->exec($sql);
	$sql = 'INSERT INTO panel(';
	$sql .= 'logo_light, ';
	$sql .= 'logo_dark, ';
	$sql .= 'logo_light_sm, ';
	$sql .= 'logo_dark_sm, ';
	$sql .= 'login_gif) ';
	$sql .= 'VALUES(';
	$sql .= '"https://i.imgur.com/6wvFmRC.png", ';
	$sql .= '"https://i.imgur.com/IrhBb1p.png", ';
	$sql .= '"https://i.imgur.com/HgTuiLC.png", ';
	$sql .= '"https://i.imgur.com/HgTuiLC.png", ';
	$sql .= '"https://i.imgur.com/Kj3bflW.gif");';
	$sqlite3->exec($sql);
}

$result = $sqlite3->query('SELECT COUNT(*) AS count FROM xc_xciptv_lic');
$row = $result->fetchArray();
$row_count = $row['count'];

if ($row_count == 0) {
	$sql = 'INSERT INTO xc_xciptv_lic(\'cid\', \'appname\') VALUES (\'521064\', \'OR Player\');';
	$sqlite3->exec($sql);
}

$result = $sqlite3->query('SELECT COUNT(*) AS count FROM ovpn_credentials');
$row = $result->fetchArray();
$row_count = $row['count'];

if ($row_count == 0) {
	$sql = 'INSERT INTO ovpn_credentials(\'ovpn_username\', \'ovpn_password\', \'surfshark_username\', \'surfshark_password\', \'ipvanish_username\', \'ipvanish_password\', \'nordvpn_username\', \'nordvpn_password\') VALUES (\'not set\', \'not set\', \'not set\', \'not set\', \'not set\', \'not set\', \'not set\', \'not set\');';
	$sqlite3->exec($sql);
}

$result = $sqlite3->query('SELECT COUNT(*) AS count FROM xc_xciptv_ads');
$row = $result->fetchArray();
$row_count = $row['count'];

if ($row_count == 0) {
	$sql = 'INSERT INTO xc_xciptv_ads(\'admob_enabled\', \'freestar_enabled\', \'prebid_enabled\', \'prebid_banner\', \'prebid_host\', \'prebid_adunitid\', \'prebid_accountid\', \'vast_enabled\', \'vast_midrollinterval\', \'vast_postrollstartat\', \'vast_vodmidrollinterval\', \'vast_vodprerollurl\', \'vast_vodmidrollurl\', \'vast_vodpostrollurl\', \'vast_seriesmidrollinterval\', \'vast_seriesprerollurl\', \'vast_seriesmidrollurl\', \'vast_seriespostrollurl\') VALUES (\'no\', \'no\', \'no\', \'\', \'\', \'\', \'\', \'no\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\');';
	$sqlite3->exec($sql);
}

$result = $sqlite3->query('SELECT COUNT(*) AS count FROM xc_xciptv_app');
$row = $result->fetchArray();
$row_count = $row['count'];

if ($row_count == 0) {
	$sql = 'INSERT INTO xc_xciptv_app(\'id\', \'appname\', \'customerid\', \'expire\', \'version_code\', \'login_type\', \'portal\', \'portal2\', \'portal3\', \'portal4\', \'portal5\', \'portal_name\', \'portal2_name\', \'portal3_name\', \'portal4_name\', \'portal5_name\', \'portal_vod\', \'portal_series\', \'apkurl\', \'backupurl\', \'logurl\', \'apkautoupdate\', \'support_email\', \'support_phone\', \'status\', \'filter_status\', \'epg_mode\', \'btn_live\', \'btn_live2\', \'btn_live3\', \'btn_live4\', \'btn_live5\', \'btn_vod\', \'btn_vod2\', \'btn_vod3\', \'btn_vod4\', \'btn_vod5\', \'btn_epg\', \'btn_epg2\', \'btn_epg3\', \'btn_epg4\', \'btn_epg5\', \'btn_series\', \'btn_series2\', \'btn_series3\', \'btn_series4\', \'btn_series5\', \'btn_radio\', \'btn_radio2\', \'btn_radio3\', \'btn_radio4\', \'btn_radio5\', \'btn_catchup\', \'btn_catchup2\', \'btn_catchup3\', \'btn_catchup4\', \'btn_catchup5\', \'btn_account\', \'btn_account2\', \'btn_account3\', \'btn_account4\', \'btn_account5\', \'btn_pr\', \'btn_rec\', \'btn_vpn\', \'btn_noti\', \'btn_update\', \'btn_login_setting\', \'btn_login_account\', \'show_expire\', \'agent\', \'all_cat\', \'stream_type\', \'player\', \'player_tv\', \'player_vod\', \'player_series\', \'player_catchup\', \'message_enable\', \'announcement_enabled\', \'updateuserinfo_enabled\', \'whatsupcheck_enabled\', \'login_logo\', \'ms\', \'ms2\', \'ms3\', \'btn_fav\', \'btn_fav2\', \'btn_fav3\', \'btn_signup\', \'bjob\', \'settings_app\', \'settings_account\', \'logs\', \'panel\', \'epg_url\', \'ovpn_url\', \'app_language\', \'load_last_channel\', \'admob_banner_id\', \'admob_interstitial_id\', \'show_cat_count\', \'activation_url\', \'send_udid\', \'ort_settings\', \'vastconfig\', \'admobconfig\', \'mnt_message\', \'mnt_status\', \'mnt_expire\', \'theme\') VALUES (\'1\', \'OR Player\', \'521064\', \'LIFETIME\', \'724\', \'login\', \'http://example.com:443/\', \'0\', \'0\', \'0\', \'0\', \'Example Portal\', \'no\', \'no\', \'no\', \'no\', \'no\', \'no\', \'http://example.com/app.apk\', \'http://example.com/api2/\', \'http://example.com/logs/\', \'no\', \'Support Email\', \'Support Phone\', \'ACTIVE\', \'No\', \'yes\', \'Yes\', \'No\', \'No\', \'No\', \'No\', \'Yes\', \'No\', \'No\', \'No\', \'No\', \'Yes\', \'No\', \'No\', \'No\', \'No\', \'Yes\', \'No\', \'No\', \'No\', \'No\', \'Yes\', \'No\', \'No\', \'No\', \'No\', \'Yes\', \'No\', \'No\', \'No\', \'No\', \'yes\', \'no\', \'no\', \'no\', \'no\', \'yes\', \'yes\', \'yes\', \'no\', \'yes\', \'yes\', \'yes\', \'yes\', \'XCIPTV\', \'yes\', \'ts\', \'EXO\', \'EXO\', \'VLC\', \'VLC\', \'VLC\', \'no\', \'no\', \'yes\', \'no\', \'yes\', \'yes\', \'no\', \'no\', \'yes\', \'no\', \'no\', \'no\', \'no\', \'yes\', \'yes\', \'yes\', \'xtreamcodes\', \'no\', \'\', \'en\', \'no\', \'no\', \'no\', \'yes\', \'no\', \'no\', \'null\', \'null\', \'null\', \'Maintenance Message\', \'INACTIVE\', \'2019-05-31 23:59:00\', \'d\');';
	$sqlite3->exec($sql);
}

$result = $sqlite3->query('SELECT COUNT(*) AS count FROM ovpn_sorting');
$row = $result->fetchArray();
$row_count = $row['count'];

if ($row_count == 0) {
	$sql = 'INSERT INTO ovpn_sorting(\'group_by\', \'order_by\') VALUES (\'id\', \'ASC\');';
	$sqlite3->exec($sql);
}

$result = $sqlite3->query('SELECT COUNT(*) AS count FROM xc_xciptv_ann');
$row = $result->fetchArray();
$row_count = $row['count'];

if ($row_count == 0) {
	$sql = 'INSERT INTO xc_xciptv_ann(\'id\', \'announcement\', \'status\', \'channel\', \'expire\', \'interval\', \'disappear\') VALUES (\'1\', \'Announcement Here\', \'INACTIVE\', \'ALL\', \'2022-12-31 00:00:00\', \'1\', \'5\');';
	$sqlite3->exec($sql);
}

$result = $sqlite3->query('SELECT COUNT(*) AS count FROM xc_ort_settings');
$row = $result->fetchArray();
$row_count = $row['count'];

if ($row_count == 0) {
	$sql = 'INSERT INTO xc_ort_settings(\'vlc_hw\', \'last_volume_vlc\', \'player_vlc_buffer\', \'video_resize_vlc\', \'video_subtitles_vlc\', \'exo_hw\', \'last_volume_exo\', \'player_exo_buffer\', \'video_resize_exo\', \'video_subtitles_exo\') VALUES (\'yes\', \'100\', \'5000\', \'0\', \'yes\', \'yes\', \'100\', \'50000\', \'0\', \'no\');';
	$sqlite3->exec($sql);
}

$result = $sqlite3->query('SELECT COUNT(*) AS count FROM tvsg_settings');
$row = $result->fetchArray();
$row_count = $row['count'];

if ($row_count == 0) {
	$sql = 'INSERT INTO tvsg_settings(\'widget_id\', \'border_color\', \'background_color\', \'text_color\', \'auto_scroll\', \'heading\') VALUES (\'5cc316f797659\', \'#1d23dd\', \'#ffffff\', \'#000000\', \'1\', \'Example Heading\');';
	$sqlite3->exec($sql);
}

$sql = 'SELECT * FROM profile ';
$sql .= 'WHERE id = \'1\';';
$result = $sqlite3->query($sql);
$profile_data = $result->fetchArray();
$sql = 'SELECT * FROM panel;';
$result = $sqlite3->query($sql);
$panel_data = $result->fetchArray();
$sql = 'SELECT * FROM snoop_logs;';
$snoop_logs = $sqlite3->query($sql);
$sql = 'SELECT * FROM tvsg_settings;';
$result = $sqlite3->query($sql);
$tvsg_settings = $result->fetchArray();
$sql = 'SELECT * FROM ottrun_messages;';
$ottrun_messages = $sqlite3->query($sql);
$sql = 'SELECT * FROM xc_ort_settings;';
$result = $sqlite3->query($sql);
$xciptv_ort_settings = $result->fetchArray();
$sql = 'SELECT * FROM xc_xciptv_ann;';
$result = $sqlite3->query($sql);
$xciptv_ann_data = $result->fetchArray();
$sql = 'SELECT * FROM xc_xciptv_lic;';
$result = $sqlite3->query($sql);
$xciptv_lic_data = $result->fetchArray();
$sql = 'SELECT * FROM xc_xciptv_ads;';
$result = $sqlite3->query($sql);
$xciptv_ads_data = $result->fetchArray();
$sql = 'SELECT * FROM xc_xciptv_app ';
$sql .= 'WHERE id = \'1\';';
$result = $sqlite3->query($sql);
$xciptv_app_data = $result->fetchArray();
$sql = 'SELECT * FROM xc_xciptv_connv2;';
$xciptv_connv2_data = $sqlite3->query($sql);
$sql = 'SELECT * FROM xc_domains_ottrun;';
$service_ottrun_data = $sqlite3->query($sql);
$sql = 'SELECT * FROM extra_customers_ottrun;';
$customer_ottrun_data = $sqlite3->query($sql);
$sql = 'SELECT * FROM ovpn_credentials;';
$ovpn_credentials = $sqlite3->query($sql);
$sql = 'SELECT * FROM ovpn_config;';
$ovpn_config = $sqlite3->query($sql);

?>