<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

echo '<script src="./assets/libs/metismenu/metisMenu.min.js"></script>' . "\r\n" . '<script src="./assets/libs/simplebar/simplebar.min.js"></script>' . "\r\n" . '<script src="./assets/libs/node-waves/waves.min.js"></script>' . "\r\n" . '<script src="./assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>' . "\r\n" . '<script src="./assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>' . "\r\n" . '<script src="./assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>' . "\r\n" . '<script src="./assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>' . "\r\n" . '<script src="./assets/libs/jszip/jszip.min.js"></script>' . "\r\n" . '<script src="./assets/libs/pdfmake/build/pdfmake.min.js"></script>' . "\r\n" . '<script src="./assets/libs/pdfmake/build/vfs_fonts.js"></script>' . "\r\n" . '<script src="./assets/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>' . "\r\n" . '<script src="./assets/libs/datatables.net-buttons/js/buttons.print.min.js"></script>' . "\r\n" . '<script src="./assets/libs/datatables.net-buttons/js/buttons.colVis.min.js"></script>' . "\r\n" . '<script src="./assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>' . "\r\n" . '<script src="./assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>' . "\r\n" . '<script src="./assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>' . "\r\n" . '<script src="./assets/libs/twitter-bootstrap-wizard/prettify.js"></script>' . "\r\n\r\n" . '<script src="./assets/js/pages/datatables.init.js"></script>' . "\r\n" . '<script src="./assets/js/pages/form-wizard.init.js"></script>';

?>