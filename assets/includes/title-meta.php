<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

include_once './assets/includes/db.php';
include_once './assets/includes/config.php';
$sql = 'SELECT `title` FROM panel;';
$result = $sqlite3->query($sql);
$panel_title = $result->fetchArray();
header('Set-Cookie: cross-site-cookie=Lax; SameSite=None; Secure');
echo "\r\n" . '<meta charset="utf-8" />' . "\r\n" . '<title>';
echo $USER_PROFILE_PANEL_EDITS ? $panel_title[0] : $USER_PROFILE_PANEL_TITLE;
echo '</title>' . "\r\n" . '<meta name="viewport" content="width=device-width, initial-scale=1.0">' . "\r\n" . '<meta content="Premium Multipurpose Admin Panel" name="description" />' . "\r\n" . '<meta content="Ian" name="author" />' . "\r\n" . '<!-- App favicon -->' . "\r\n" . '<link rel="shortcut icon" href="assets/images/favicon.ico">';

?>