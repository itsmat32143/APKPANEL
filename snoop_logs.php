<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

session_start();
include 'assets/includes/db.php';
include 'assets/includes/config.php';

if ($_ERRORS) {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}
if (!isset($_SESSION['loggedin']) && !$_SESSION['loggedin']) {
	header('location: logout.php');
}

if (isset($_GET['delete_log'])) {
	$sql = 'DELETE FROM snoop_logs ';
	$sql .= 'WHERE id = ' . $_GET['delete_log'] . ';';
	$sqlite3->exec($sql);
	header('location: snoop_logs.php');
}

if (isset($_GET['delete_all'])) {
	$sql = 'DELETE FROM snoop_logs;';
	$sqlite3->exec($sql);
	header('location: snoop_logs.php');
}

echo '<!doctype html>' . "\r\n" . '<html lang="en">' . "\r\n\r\n" . '<head>' . "\r\n\r\n" . '    ';
include 'assets/includes/title-meta.php';
echo "\r\n" . '    ';
include 'assets/includes/head-css.php';
echo "\r\n" . '</head>' . "\r\n\r\n" . '<body data-sidebar="dark">' . "\r\n\r\n" . '    <!-- Loader -->' . "\r\n" . '    <div id="preloader">' . "\r\n" . '        <div id="status">' . "\r\n" . '            <div class="spinner">' . "\r\n" . '                <i class="ri-loader-line spin-icon"></i>' . "\r\n" . '            </div>' . "\r\n" . '        </div>' . "\r\n" . '    </div>' . "\r\n\r\n" . '    <div id="layout-wrapper">' . "\r\n\r\n" . '        ';
include 'assets/includes/topbar.php';
echo "\r\n" . '        ';
include 'assets/includes/sidebar.php';
echo "\r\n" . '        <div class="main-content">' . "\r\n\r\n" . '            <div class="page-content">' . "\r\n" . '                <div class="container-fluid">' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="page-title-box d-flex align-items-center justify-content-between">' . "\r\n" . '                                <h4 class="mb-0">Multipurpose Panel</h4>' . "\r\n\r\n" . '                                <div class="page-title-right">' . "\r\n" . '                                    <ol class="breadcrumb m-0">' . "\r\n" . '                                        <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>' . "\r\n" . '                                        <li class="breadcrumb-item active">Snoop Logs</li>' . "\r\n" . '                                    </ol>' . "\r\n" . '                                </div>' . "\r\n\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <h4 class="card-title">Snoop Logs</h4>' . "\r\n" . '                                    <p class="card-title-desc">Check connections to the Admin panel from unauthorised users.' . "\r\n" . '                                        <a type="button" href="./snoop_logs.php?delete_all=1" class="btn-sm btn-danger waves-effect waves-light float-right"><i class="dripicons-document-delete"></i> Clear all logs</a>' . "\r\n" . '                                    </p>' . "\r\n\r\n" . '                                    <br />' . "\r\n\r\n" . '                                    <div class="table-responsive">' . "\r\n" . '                                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">' . "\r\n" . '                                            <thead>' . "\r\n" . '                                                <tr>' . "\r\n" . '                                                    <th>IP Address</th>' . "\r\n" . '                                                    <th>Date</th>' . "\r\n" . '                                                    <th></th>' . "\r\n" . '                                                </tr>' . "\r\n" . '                                            </thead>' . "\r\n" . '                                            ';

while ($row = $snoop_logs->fetchArray()) {
	echo '                                                <tr>' . "\r\n" . '                                                    <td>';
	echo $row['ip'];
	echo '</td>' . "\r\n" . '                                                    <td>';
	echo $row['date'];
	echo '</td>' . "\r\n" . '                                                    <td>' . "\r\n" . '                                                        <a type="button" href="./snoop_logs.php?delete_log=';
	echo $row['id'];
	echo '" class="btn-sm btn-danger waves-effect waves-light"><i class="dripicons-document-delete"></i></a>' . "\r\n" . '                                                    </td>' . "\r\n" . '                                                </tr>' . "\r\n" . '                                            ';
}

echo '                                        </table>' . "\r\n" . '                                    </div>' . "\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n" . '                </div>' . "\r\n" . '            </div>' . "\r\n\r\n" . '            ';
include 'assets/includes/footer.php';
echo "\r\n" . '        </div>' . "\r\n\r\n" . '    </div>' . "\r\n\r\n" . '    ';
include 'assets/includes/right-sidebar.php';
echo "\r\n" . '    ';
include 'assets/includes/vendor-scripts.php';
echo "\r\n" . '    <script src="assets/js/app.js"></script>' . "\r\n\r\n" . '</body>' . "\r\n\r\n" . '</html>';

?>