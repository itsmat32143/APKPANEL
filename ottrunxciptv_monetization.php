<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

session_start();
include 'assets/includes/db.php';
include 'assets/includes/config.php';

if ($_ERRORS) {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}

date_default_timezone_set('UTC');
if (!isset($_SESSION['loggedin']) && !$_SESSION['loggedin']) {
	header('location: logout.php');
}

if (isset($_POST['admob_submit'])) {
	$sql = 'UPDATE xc_xciptv_ads SET ';
	$sql .= 'admob_enabled = \'' . $_POST['admob_enabled'] . '\';';
	$sqlite3->exec($sql);
	$sql = 'UPDATE xc_xciptv_app SET ';
	$sql .= 'admob_banner_id = \'' . $_POST['admob_banner_id'] . '\', ';
	$sql .= 'admob_interstitial_id = \'' . $_POST['admob_interstitial_id'] . '\';';
	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_messages.php');
}

if (isset($_POST['freestar_submit'])) {
	$sql = 'UPDATE xc_xciptv_ads SET ';
	$sql .= 'freestar_enabled = \'' . $_POST['freestar_enabled'] . '\';';
	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_messages.php');
}

if (isset($_POST['prebid_submit'])) {
	$sql = 'UPDATE xc_xciptv_ads SET ';
	$sql .= 'prebid_enabled = \'' . $_POST['prebid_enabled'] . '\',';
	$sql .= 'prebid_banner = \'' . $_POST['prebid_banner'] . '\',';
	$sql .= 'prebid_host = \'' . $_POST['prebid_host'] . '\',';
	$sql .= 'prebid_adunitid = \'' . $_POST['prebid_adunitid'] . '\',';
	$sql .= 'prebid_accountid = \'' . $_POST['prebid_accountid'] . '\';';
	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_messages.php');
}

if (isset($_POST['vast_submit'])) {
	$sql = 'UPDATE xc_xciptv_ads SET ';
	$sql .= 'vast_enabled = \'' . $_POST['vast_enabled'] . '\',';
	$sql .= 'vast_midrollinterval = \'' . $_POST['vast_midrollinterval'] . '\',';
	$sql .= 'vast_postrollstartat = \'' . $_POST['vast_postrollstartat'] . '\',';
	$sql .= 'vast_vodmidrollinterval = \'' . $_POST['vast_vodmidrollinterval'] . '\',';
	$sql .= 'vast_vodprerollurl = \'' . $_POST['vast_vodprerollurl'] . '\',';
	$sql .= 'vast_vodmidrollurl = \'' . $_POST['vast_vodmidrollurl'] . '\',';
	$sql .= 'vast_vodpostrollurl = \'' . $_POST['vast_vodpostrollurl'] . '\',';
	$sql .= 'vast_seriesmidrollinterval = \'' . $_POST['vast_seriesmidrollinterval'] . '\',';
	$sql .= 'vast_seriesprerollurl = \'' . $_POST['vast_seriesprerollurl'] . '\',';
	$sql .= 'vast_seriesmidrollurl = \'' . $_POST['vast_seriesmidrollurl'] . '\',';
	$sql .= 'vast_seriespostrollurl = \'' . $_POST['vast_seriespostrollurl'] . '\';';
	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_messages.php');
}

echo '<!doctype html>' . "\r\n" . '<html lang="en">' . "\r\n\r\n" . '<head>' . "\r\n\r\n" . '    ';
include 'assets/includes/title-meta.php';
echo "\r\n" . '    ';
include 'assets/includes/head-css.php';
echo "\r\n" . '</head>' . "\r\n\r\n" . '<body data-sidebar="dark">' . "\r\n\r\n" . '    <!-- Loader -->' . "\r\n" . '    <div id="preloader">' . "\r\n" . '        <div id="status">' . "\r\n" . '            <div class="spinner">' . "\r\n" . '                <i class="ri-loader-line spin-icon"></i>' . "\r\n" . '            </div>' . "\r\n" . '        </div>' . "\r\n" . '    </div>' . "\r\n\r\n" . '    <div id="layout-wrapper">' . "\r\n\r\n" . '        ';
include 'assets/includes/topbar.php';
echo "\r\n" . '        ';
include 'assets/includes/sidebar.php';
echo "\r\n" . '        <div class="main-content">' . "\r\n\r\n" . '            <div class="page-content">' . "\r\n" . '                <div class="container-fluid">' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="page-title-box d-flex align-items-center justify-content-between">' . "\r\n" . '                                <h4 class="mb-0"> </h4>' . "\r\n\r\n" . '                                <div class="page-title-right">' . "\r\n" . '                                    <ol class="breadcrumb m-0">' . "\r\n" . '                                        <li class="breadcrumb-item"><a href="javascript: void(0);">OTTRun XCIPTV</a></li>' . "\r\n" . '                                        <li class="breadcrumb-item active">Monetization & Ads</li>' . "\r\n" . '                                    </ol>' . "\r\n" . '                                </div>' . "\r\n\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n\r\n" . '                    <div class="alert alert-danger alert-dismissible fade show col-6 mx-auto" role="alert">' . "\r\n" . '                            <center>' . "\r\n" . '                                Please note that some manual editing of the Application is required to fully enable some </br>Ad services and that some Ad services only work with >723 Applications.' . "\r\n" . '                            </center>' . "\r\n" . '                        </div>' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        ' . "\r\n\r\n" . '                        <div class="col-6 mx-auto">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <h4 class="card-title">Google AdMob</h4>' . "\r\n" . '                                    <p class="card-title-desc"><a href="https://admob.google.com/home/">AdMob</a>’s robust reporting and measurement features deliver deeper insights into how your users are interacting with your mobile app and ads.</p>' . "\r\n\r\n" . '                                    <br />' . "\r\n\r\n" . '                                    <form method="post">' . "\r\n" . '                                        <div class="form-group">' . "\r\n" . '                                            <label for="admob_enabled">AdMob Enabled</label>' . "\r\n" . '                                            <select class="form-control" id="admob_enabled" name="admob_enabled">' . "\r\n" . '                                                <option value="no" ';
echo $xciptv_ads_data['admob_enabled'] == 'no' ? 'selected' : '';
echo '>no</option>' . "\r\n" . '                                                <option value="yes" ';
echo $xciptv_ads_data['admob_enabled'] == 'yes' ? 'selected' : '';
echo '>yes</option>' . "\r\n" . '                                            </select>' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-left">' . "\r\n" . '                                            <label for="admob_banner_id">Banner ID</label>' . "\r\n" . '                                            <input class="form-control" id="admob_banner_id" name="admob_banner_id" value="';
echo $xciptv_app_data['admob_banner_id'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-left">' . "\r\n" . '                                            <label for="admob_interstitial_id">Interstitial ID</label>' . "\r\n" . '                                            <input class="form-control" id="admob_interstitial_id" name="admob_interstitial_id" value="';
echo $xciptv_app_data['admob_interstitial_id'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group">' . "\r\n" . '                                            <button class="btn btn-primary" name="admob_submit" type="submit">Save</button>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </form>' . "\r\n\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n\r\n" . '                        <div class="col-6 mx-auto">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <h4 class="card-title">Freestar</h4>' . "\r\n" . '                                    <p class="card-title-desc"><a href="https://freestar.com/">Freestar</a> is a Top 15 Comscore property, exclusively reaching over 200 million monthly unique visitors.</p>' . "\r\n\r\n" . '                                    <br />' . "\r\n" . '                                    ' . "\r\n" . '                                    <p>Note: you must register your application with <a href="https://freestar.com/">Freestar</a> to show this type of advert. You can read more about their in-app solution <a href="https://freestar.com/solutions/in-app/">here.</a></p>' . "\r\n" . '                                    ' . "\r\n" . '                                    <br />' . "\r\n\r\n" . '                                    <form method="post">' . "\r\n" . '                                        <div class="form-group">' . "\r\n" . '                                            <label for="freestar_enabled">Freestar Enabled</label>' . "\r\n" . '                                            <select class="form-control" id="freestar_enabled" name="freestar_enabled">' . "\r\n" . '                                                <option value="no" ';
echo $xciptv_ads_data['freestar_enabled'] == 'no' ? 'selected' : '';
echo '>no</option>' . "\r\n" . '                                                <option value="yes" ';
echo $xciptv_ads_data['freestar_enabled'] == 'yes' ? 'selected' : '';
echo '>yes</option>' . "\r\n" . '                                            </select>' . "\r\n" . '                                        </div>' . "\r\n" . '                                        <div class="form-group">' . "\r\n" . '                                            <button class="btn btn-primary" name="freestar_submit" type="submit">Save</button>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </form>' . "\r\n\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n\r\n" . '                    </div>' . "\r\n\r\n" . '                    <div class="row">' . "\r\n\r\n" . '                        <div class="col-6 mx-auto">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <h4 class="card-title">Prebid</h4>' . "\r\n" . '                                    <p class="card-title-desc"><a href="https://prebid.org/">Prebid</a> drives additional demand from leading ad exchanges straight to your ad server.</p>' . "\r\n\r\n" . '                                    <br />' . "\r\n\r\n" . '                                    <form method="post">' . "\r\n" . '                                        <div class="form-group">' . "\r\n" . '                                            <label for="prebid_banner">Banner</label>' . "\r\n" . '                                            <input class="form-control" id="prebid_banner" name="prebid_banner" value="';
echo $xciptv_ads_data['prebid_banner'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-right">' . "\r\n" . '                                            <label for="prebid-host">Host</label>' . "\r\n" . '                                            <input class="form-control" id="prebid_host" name="prebid_host" value="';
echo $xciptv_ads_data['prebid_host'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-left">' . "\r\n" . '                                            <label for="prebid_adunitid">Ad Unit ID</label>' . "\r\n" . '                                            <input class="form-control" id="prebid_adunitid" name="prebid_adunitid" value="';
echo $xciptv_ads_data['prebid_adunitid'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-left">' . "\r\n" . '                                            <label for="prebid_accountid">Account ID</label>' . "\r\n" . '                                            <input class="form-control" id="prebid_accountid" name="prebid_accountid" value="';
echo $xciptv_ads_data['prebid_accountid'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-right">' . "\r\n" . '                                            <label for="prebid_enabled">Prebid Enabled</label>' . "\r\n" . '                                            <select class="form-control" id="prebid_enabled" name="prebid_enabled">' . "\r\n" . '                                                <option value="no" ';
echo $xciptv_ads_data['prebid_enabled'] == 'no' ? 'selected' : '';
echo '>no</option>' . "\r\n" . '                                                <option labelvalue="yes" ';
echo $xciptv_ads_data['prebid_enabled'] == 'yes' ? 'selected' : '';
echo '>yes</option>' . "\r\n" . '                                            </select>' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group">' . "\r\n" . '                                            <button class="btn btn-primary" name="prebid_submit" type="submit">Save</button>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </form>' . "\r\n\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n\r\n" . '                        <div class="col-6 mx-auto">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <h4 class="card-title">VAST</h4>' . "\r\n" . '                                    <p class="card-title-desc">A <a href="https://support.google.com/admanager/answer/10678356">VAST</a> ad tag URL is used by a player to retrieve video and audio ads.</p>' . "\r\n\r\n" . '                                    <br />' . "\r\n\r\n" . '                                    <form method="post">' . "\r\n" . '                                        <div class="form-group col-6 float-left">' . "\r\n" . '                                            <label for="vast_midrollinterval">Mid-roll interval</label>' . "\r\n" . '                                            <input class="form-control" id="vast_midrollinterval" name="vast_midrollinterval" value="';
echo $xciptv_ads_data['vast_midrollinterval'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-right">' . "\r\n" . '                                            <label for="vast_postrollstartat">Post-roll start at</label>' . "\r\n" . '                                            <input class="form-control" id="vast_postrollstartat" name="vast_postrollstartat" value="';
echo $xciptv_ads_data['vast_postrollstartat'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-right">' . "\r\n" . '                                            <label for="vast_vodmidrollinterval">VOD Mid-roll interval</label>' . "\r\n" . '                                            <input class="form-control" id="vast_vodmidrollinterval" name="vast_vodmidrollinterval" value="';
echo $xciptv_ads_data['vast_vodmidrollinterval'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-right">' . "\r\n" . '                                            <label for="vast_vodprerollurl">VOD Pre-roll url</label>' . "\r\n" . '                                            <input class="form-control" id="vast_vodprerollurl" name="vast_vodprerollurl" value="';
echo $xciptv_ads_data['vast_vodprerollurl'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-right">' . "\r\n" . '                                            <label for="vast_vodmidrollurl">VOD Mid-roll url</label>' . "\r\n" . '                                            <input class="form-control" id="vast_vodmidrollurl" name="vast_vodmidrollurl" value="';
echo $xciptv_ads_data['vast_vodmidrollurl'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-right">' . "\r\n" . '                                            <label for="vast_vodpostrollurl">VOD Post-roll url</label>' . "\r\n" . '                                            <input class="form-control" id="vast_vodpostrollurl" name="vast_vodpostrollurl" value="';
echo $xciptv_ads_data['vast_vodpostrollurl'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-left">' . "\r\n" . '                                            <label for="vast_seriesmidrollinterval">Series Mid-roll interval</label>' . "\r\n" . '                                            <input class="form-control" id="vast_seriesmidrollinterval" name="vast_seriesmidrollinterval" value="';
echo $xciptv_ads_data['vast_seriesmidrollinterval'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-left">' . "\r\n" . '                                            <label for="vast_seriesprerollurl">Series Pre-roll url</label>' . "\r\n" . '                                            <input class="form-control" id="vast_seriesprerollurl" name="vast_seriesprerollurl" value="';
echo $xciptv_ads_data['vast_seriesprerollurl'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-left">' . "\r\n" . '                                            <label for="vast_seriesmidrollurl">Series Mid-roll url</label>' . "\r\n" . '                                            <input class="form-control" id="vast_seriesmidrollurl" name="vast_seriesmidrollurl" value="';
echo $xciptv_ads_data['vast_seriesmidrollurl'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-left">' . "\r\n" . '                                            <label for="vast_seriespostrollurl">Series Post-roll url</label>' . "\r\n" . '                                            <input class="form-control" id="vast_seriespostrollurl" name="vast_seriespostrollurl" value="';
echo $xciptv_ads_data['vast_seriespostrollurl'];
echo '" type="text" />' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group col-6 float-right">' . "\r\n" . '                                            <label for="vast_enabled">VAST Enabled</label>' . "\r\n" . '                                            <select class="form-control" id="vast_enabled" name="vast_enabled">' . "\r\n" . '                                                <option value="no" ';
echo $xciptv_ads_data['vast_enabled'] == 'no' ? 'selected' : '';
echo '>no</option>' . "\r\n" . '                                                <option labelvalue="yes" ';
echo $xciptv_ads_data['vast_enabled'] == 'yes' ? 'selected' : '';
echo '>yes</option>' . "\r\n" . '                                            </select>' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="form-group">' . "\r\n" . '                                            <button class="btn btn-primary" name="vast_submit" type="submit">Save</button>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </form>' . "\r\n\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n\r\n" . '                    </div>' . "\r\n\r\n" . '                </div>' . "\r\n" . '            </div>' . "\r\n\r\n" . '            ';
include 'assets/includes/footer.php';
echo '        </div>' . "\r\n\r\n" . '    </div>' . "\r\n\r\n" . '    ';
include 'assets/includes/right-sidebar.php';
echo "\r\n" . '    ';
include 'assets/includes/vendor-scripts.php';
echo "\r\n" . '    <script src="./assets/js/app.js"></script>' . "\r\n\r\n" . '</body>' . "\r\n\r\n" . '</html>';

?>