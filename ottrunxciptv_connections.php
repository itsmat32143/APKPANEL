<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

session_start();
include 'assets/includes/db.php';
include 'assets/includes/config.php';

if ($_ERRORS) {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}
if (!isset($_SESSION['loggedin']) && !$_SESSION['loggedin']) {
	header('location: logout.php');
}

echo '<!doctype html>' . "\r\n" . '<html lang="en">' . "\r\n\r\n" . '<head>' . "\r\n\r\n" . '    ';
include 'assets/includes/title-meta.php';
echo "\r\n" . '    ';
include 'assets/includes/head-css.php';
echo "\r\n" . '</head>' . "\r\n\r\n" . '<body data-sidebar="dark">' . "\r\n\r\n" . '    <!-- Loader -->' . "\r\n" . '    <div id="preloader">' . "\r\n" . '        <div id="status">' . "\r\n" . '            <div class="spinner">' . "\r\n" . '                <i class="ri-loader-line spin-icon"></i>' . "\r\n" . '            </div>' . "\r\n" . '        </div>' . "\r\n" . '    </div>' . "\r\n\r\n" . '    <div id="layout-wrapper">' . "\r\n\r\n" . '        ';
include 'assets/includes/topbar.php';
echo "\r\n" . '        ';
include 'assets/includes/sidebar.php';
echo "\r\n" . '        <div class="main-content">' . "\r\n\r\n" . '            <div class="page-content">' . "\r\n" . '                <div class="container-fluid">' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="page-title-box d-flex align-items-center justify-content-between">' . "\r\n" . '                                <h4 class="mb-0"> </h4>' . "\r\n\r\n" . '                                <div class="page-title-right">' . "\r\n" . '                                    <ol class="breadcrumb m-0">' . "\r\n" . '                                        <li class="breadcrumb-item"><a href="javascript: void(0);">OTTRun XCIPTV</a></li>' . "\r\n" . '                                        <li class="breadcrumb-item active">Connections</li>' . "\r\n" . '                                    </ol>' . "\r\n" . '                                </div>' . "\r\n\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <h4 class="card-title">Application Connections</h4>' . "\r\n" . '                                    <p class="card-title-desc">View the connection history of the applications.</p>' . "\r\n\r\n" . '                                    <br />' . "\r\n\r\n" . '                                    <div class="col-12 d-flex mx-auto">' . "\r\n\r\n" . '                                        <div class="card-body">' . "\r\n" . '                                            <div class="col-md-12">' . "\r\n\r\n" . '                                                <div class="row">' . "\r\n" . '                                                    <div class="card col-md-12">' . "\r\n" . '                                                        <br>' . "\r\n" . '                                                        <table id="datatable-buttons" class="table table-bordered">' . "\r\n" . '                                                            <thead>' . "\r\n" . '                                                                <tr>' . "\r\n" . '                                                                    <th>User</th>' . "\r\n" . '                                                                    <th>App name</th>' . "\r\n" . '                                                                    <th>App Package</th>' . "\r\n" . '                                                                    <th>Device Type</th>' . "\r\n" . '                                                                    <th>Version</th>' . "\r\n" . '                                                                    <th hidden>appid</th>' . "\r\n" . '                                                                    <th hidden>customerid</th>' . "\r\n" . '                                                                    <th hidden>did</th>' . "\r\n" . '                                                                    <th>Datetime</th>' . "\r\n" . '                                                                    <th>Actions</th>' . "\r\n" . '                                                                </tr>' . "\r\n" . '                                                            </thead>' . "\r\n" . '                                                            <tbody>' . "\r\n" . '                                                            ';

while ($row = $xciptv_connv2_data->fetchArray()) {
	echo '                                                                <tr>' . "\r\n" . '                                                                    <td>';
	echo $row['userid'];
	echo '</td>' . "\r\n" . '                                                                    <td>';
	echo $row['an'];
	echo '</td>' . "\r\n" . '                                                                    <td>';
	echo $row['p'];
	echo '</td>' . "\r\n" . '                                                                    <td>';
	echo $row['device_type'];
	echo '</td>' . "\r\n" . '                                                                    <td>';
	echo $row['version'];
	echo '</td>' . "\r\n" . '                                                                    <td hidden>';
	echo $row['appid'];
	echo '</td>' . "\r\n" . '                                                                    <td hidden>';
	echo $row['customerid'];
	echo '</td>' . "\r\n" . '                                                                    <td hidden>';
	echo $row['did'];
	echo '</td>' . "\r\n" . '                                                                    <td>';
	echo $row['datetime'];
	echo '</td>' . "\r\n" . '                                                                    <td>' . "\r\n" . '                                                                        <a href="./ottrunxciptv_messages.php?msg=';
	echo $row['userid'];
	echo '" class="btn btn-primary btn-sm"><i class="ri-message-line"></i> Message</a>' . "\r\n" . '                                                                    </td>' . "\r\n" . '                                                                </tr>' . "\r\n" . '                                                            ';
}

echo '                                                            </tbody>' . "\r\n" . '                                                        </table>' . "\r\n" . '                                                        <br>' . "\r\n" . '                                                    </div>' . "\r\n" . '                                                </div>' . "\r\n\r\n" . '                                            </div>' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                    </div>' . "\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '                </div>' . "\r\n" . '            </div>' . "\r\n\r\n" . '            ';
include 'assets/includes/footer.php';
echo '        </div>' . "\r\n\r\n" . '    </div>' . "\r\n\r\n" . '    ';
include 'assets/includes/right-sidebar.php';
echo "\r\n" . '    ';
include 'assets/includes/vendor-scripts.php';
echo "\r\n" . '    <script src="./assets/js/app.js"></script>' . "\r\n\r\n" . '</body>' . "\r\n\r\n" . '</html>';

?>