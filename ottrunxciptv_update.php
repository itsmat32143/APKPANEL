<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

session_start();
include 'assets/includes/db.php';
include 'assets/includes/config.php';

if ($_ERRORS) {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}
if (!isset($_SESSION['loggedin']) && !$_SESSION['loggedin']) {
	header('location: logout.php');
}

if (isset($_POST['update_submit'])) {
	$sql = 'UPDATE xc_xciptv_app SET ';
	$sql .= 'version_code = \'' . $_POST['version_code'] . '\', ';
	$sql .= 'apkautoupdate = \'' . $_POST['apkautoupdate'] . '\', ';
	$sql .= 'apkurl = \'' . $_POST['apkurl'] . '\';';
	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_update.php');
}

echo '<!doctype html>' . "\r\n" . '<html lang="en">' . "\r\n\r\n" . '<head>' . "\r\n\r\n" . '    ';
include 'assets/includes/title-meta.php';
echo "\r\n" . '    ';
include 'assets/includes/head-css.php';
echo "\r\n" . '</head>' . "\r\n\r\n" . '<body data-sidebar="dark">' . "\r\n\r\n" . '    <!-- Loader -->' . "\r\n" . '    <div id="preloader">' . "\r\n" . '        <div id="status">' . "\r\n" . '            <div class="spinner">' . "\r\n" . '                <i class="ri-loader-line spin-icon"></i>' . "\r\n" . '            </div>' . "\r\n" . '        </div>' . "\r\n" . '    </div>' . "\r\n\r\n" . '    <div id="layout-wrapper">' . "\r\n\r\n" . '        ';
include 'assets/includes/topbar.php';
echo "\r\n" . '        ';
include 'assets/includes/sidebar.php';
echo "\r\n" . '        <div class="main-content">' . "\r\n\r\n" . '            <div class="page-content">' . "\r\n" . '                <div class="container-fluid">' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="page-title-box d-flex align-items-center justify-content-between">' . "\r\n" . '                                <h4 class="mb-0"> </h4>' . "\r\n\r\n" . '                                <div class="page-title-right">' . "\r\n" . '                                    <ol class="breadcrumb m-0">' . "\r\n" . '                                        <li class="breadcrumb-item"><a href="javascript: void(0);">OTTRun XCIPTV</a></li>' . "\r\n" . '                                        <li class="breadcrumb-item active">Remote Update</li>' . "\r\n" . '                                    </ol>' . "\r\n" . '                                </div>' . "\r\n\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <h4 class="card-title">Remote Application Update</h4>' . "\r\n" . '                                    <p class="card-title-desc">Update your application remotely from a direct URL.</p>' . "\r\n\r\n" . '                                    <div class="alert alert-warning alert-dismissible fade show" role="alert">' . "\r\n" . '                                        <center>Any application version post 12th February 2022 no longer support remote updates due to Google guidelines.</center>' . "\r\n" . '                                    </div>' . "\r\n\r\n" . '                                    <br />' . "\r\n\r\n" . '                                    <div class="col-4"></div>' . "\r\n\r\n" . '                                    <div class="col-4 d-flex mx-auto">' . "\r\n\r\n" . '                                        <div class="card-body">' . "\r\n" . '                                            <form method="POST">' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label for="version_code">Application Version Number</label>' . "\r\n" . '                                                    <input class="form-control" id="description" name="version_code" value="';
echo $xciptv_app_data['version_code'];
echo '" type="text" />' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label for="apkautoupdate">Enable forced updates</label>' . "\r\n" . '                                                    <select class="form-control" id="select" name="apkautoupdate">' . "\r\n" . '                                                        <option value="yes" ';
echo $xciptv_app_data['apkautoupdate'] == 'yes' ? 'selected' : '';
echo '>yes</option>' . "\r\n" . '                                                        <option value="no" ';
echo $xciptv_app_data['apkautoupdate'] == 'no' ? 'selected' : '';
echo '>no</option>' . "\r\n" . '                                                    </select>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label for="apkurl">Application URL (direct)</label>' . "\r\n" . '                                                    <input class="form-control" id="description" name="apkurl" value="';
echo $xciptv_app_data['apkurl'];
echo '" type="text" />' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <button class="btn btn-primary" name="update_submit" type="update_submit">Save</button>' . "\r\n" . '                                                </div>' . "\r\n" . '                                            </form>' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                    </div>' . "\r\n\r\n" . '                                    <div class="col-4"></div>' . "\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '                </div>' . "\r\n" . '            </div>' . "\r\n\r\n" . '            ';
include 'assets/includes/footer.php';
echo '        </div>' . "\r\n\r\n" . '    </div>' . "\r\n\r\n" . '    ';
include 'assets/includes/right-sidebar.php';
echo "\r\n" . '    ';
include 'assets/includes/vendor-scripts.php';
echo "\r\n" . '    <script src="./assets/js/app.js"></script>' . "\r\n\r\n" . '</body>' . "\r\n\r\n" . '</html>';

?>