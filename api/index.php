<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

function get_ip_address()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
		return $_SERVER['HTTP_CLIENT_IP'];
	}

	if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
			$iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);

			foreach ($iplist as $ip) {
				if (validate_ip($ip)) {
					return $ip;
				}
			}
		}
		else if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
	}
	if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED'])) {
		return $_SERVER['HTTP_X_FORWARDED'];
	}
	if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
		return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
	}
	if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR'])) {
		return $_SERVER['HTTP_FORWARDED_FOR'];
	}
	if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED'])) {
		return $_SERVER['HTTP_FORWARDED'];
	}

	return $_SERVER['REMOTE_ADDR'];
}

function validate_ip($ip)
{
	if (strtolower($ip) === 'unknown') {
		return false;
	}

	$ip = ip2long($ip);
	if ((false !== $ip) && (-1 !== $ip)) {
		$ip = sprintf('%u', $ip);
		if ((0 <= $ip) && ($ip <= 50331647)) {
			return false;
		}
		if ((167772160 <= $ip) && ($ip <= 184549375)) {
			return false;
		}
		if ((2130706432 <= $ip) && ($ip <= 2147483647)) {
			return false;
		}
		if ((2851995648.0 <= $ip) && ($ip <= 2852061183.0)) {
			return false;
		}
		if ((2886729728.0 <= $ip) && ($ip <= 2887778303.0)) {
			return false;
		}
		if ((3221225984.0 <= $ip) && ($ip <= 3221226239.0)) {
			return false;
		}
		if ((3232235520.0 <= $ip) && ($ip <= 3232301055.0)) {
			return false;
		}

		if (4294967040.0 <= $ip) {
			return false;
		}
	}

	return true;
}

function get_operating_system()
{
	global $user_agent;
	$os_platform = 'Unknown';
	$os_array = ['/windows nt 10/i' => 'Windows', '/macintosh|mac os x/i' => 'Mac', '/linux/i' => 'Linux', '/ubuntu/i' => 'Linux', '/iphone/i' => 'iOS', '/ipad/i' => 'iOS', '/android/i' => 'Android', '/webos/i' => 'Android'];

	foreach ($os_array as $regex => $value) {
		if (preg_match($regex, $user_agent)) {
			$os_platform = $value;
		}
	}

	return $os_platform;
}

function get_browser_type()
{
	global $user_agent;
	$browser = 'Unknown';
	$browser_array = ['/msie/i' => 'Internet Explorer', '/firefox/i' => 'Firefox', '/safari/i' => 'Safari', '/chrome/i' => 'Chrome', '/edge/i' => 'Edge', '/opera/i' => 'Opera'];

	foreach ($browser_array as $regex => $value) {
		if (preg_match($regex, $user_agent)) {
			$browser = $value;
		}
	}

	return $browser;
}

$user_agent = $_SERVER['HTTP_USER_AGENT'];
$visitor_ip = get_ip_address();
$visitor_details = json_decode(file_get_contents('http://ipinfo.io/' . $visitor_ip . '/json'));
@$visitor_country = $visitor_details->country;
@$visitor_city = $visitor_details->city;
@$visitor_region = $visitor_details->region;
@$visitor_isp = $visitor_details->org;
$visitor_isp = preg_replace('/AS\\d{1,}\\s/', '', $visitor_isp);
@$visitor_location = $visitor_details->loc;
$visitor_browser = get_browser_type();
$visitor_os = get_operating_system();
$db = new SQLite3('./.cockpit-0001.db');
$sql = 'INSERT INTO snoop_logs(';
$sql .= 'ip, ';
$sql .= 'date) ';
$sql .= 'VALUES(';
$sql .= '"' . $visitor_ip . '", ';
$sql .= '"' . date('d/m/Y H:i:s') . '");';
$db->exec($sql);
echo "\r\n" . '<link href="../assets/css/403.min.css" rel="stylesheet" type="text/css" />' . "\r\n\r\n" . '<h1>403</h1>' . "\r\n" . '<div>' . "\r\n" . '    <p>> <span>ERROR CODE</span>: "<i>HTTP 403 Forbidden</i>"</p>' . "\r\n" . '    <p>> <span>ERROR DESCRIPTION</span>: "<i>Access Denied. You Do Not Have The Permission To Access This Page On This' . "\r\n" . '            Server</i>"</p>' . "\r\n" . '    <p>> <span>BROWSER</span>: [';
echo $visitor_browser;
echo ']</p>' . "\r\n" . '    <p>> <span>OPERATING SYSTEM</span>: [';
echo $visitor_os;
echo ']</p>' . "\r\n" . '    <p>> <span>IP ADDRESS</span>: [';
echo get_ip_address();
echo ']</p>' . "\r\n" . '    <p>> <span>COUNTRY</span>: [';
echo $visitor_country;
echo ']</p>' . "\r\n" . '    <p>> <span>REGION</span>: [';
echo $visitor_region;
echo ']</p>' . "\r\n" . '    <p>> <span>CITY</span>: [';
echo $visitor_city;
echo ']</p>' . "\r\n" . '    <p>> <span>LOCATION</span>: [';
echo $visitor_location;
echo ']</p>' . "\r\n" . '    <p>> <span>ISP</span>: [';
echo $visitor_isp;
echo ']</p>' . "\r\n" . '    <p>> <span>USER-AGENT</span>: [';
echo $user_agent;
echo ']</p>' . "\r\n\r\n" . '    <p>> <span>YOUR INFORMATION HAS BEEN LOGGED. ❤</span></p>' . "\r\n" . '</div>' . "\r\n\r\n" . '<script src="../assets/js/403.js"></script>';

?>