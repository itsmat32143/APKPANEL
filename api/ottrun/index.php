<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

include 'functions.php';
error_reporting(0);
auth1();
$dnsList = [];
$pkg = (isset($_REQUEST['pkg']) ? $_REQUEST['pkg'] : NULL);

if ($pkg === 'com.ottrun.orvpn') {
	array_push($dnsList, new XcPortal((isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] !== 'off') ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/'));
}

echo encrypt($dnsList, PASSWORD);

?>