<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

class UserInfoConfig
{
	public $username = '';
	public $password = '';
	public $message = 'https://https://domain//';
	public $auth = '0';
	public $status = '0';
	public $exp_date = '4092325092';
	public $is_trial = '0';
	public $active_cons = '0';
	public $created_at = '612835200';
	public $max_connections = '1';
	public $allowed_output_formats = ['m3u8', 'ts'];

	public function __construct($username = '', $password = '', $auth = '0', $status = '0', $exp_date = NULL)
	{
		$this->username = $username;
		$this->password = $password;
		$this->auth = $auth;
		$this->status = $status;
		$this->exp_date = $exp_date;
	}
}

class ServerInfoConfig
{
	public $url = 'https://https://domain//';
	public $port = '80';
	public $https_port = '443';
	public $server_protocol = 'https';
	public $rtmp_port = '0';
	public $timezone = 'UTC';
	public $timestamp_now = '1662410140';
	public $time_now = '2022-09-05 20:35:40';
	public $process = true;

	public function __construct()
	{
	}
}

function json_response($user_info = NULL, $server_info = NULL)
{
	header_remove();
	http_response_code(200);
	header('Cache-Control: no-transform,public,max-age=300,s-maxage=900');
	header('Status: 200');
	return json_encode(['user_info' => $user_info, 'server_info' => $server_info]);
}

date_default_timezone_set('UTC');
$db = new SQLite3('../../api/.cockpit-0001.db');
if (isset($_GET['username']) && isset($_GET['password'])) {
	$sql = 'SELECT * FROM extra_customers_ottrun ';
	$sql .= 'WHERE username="' . $_GET['username'] . '";';
	$customer_data = $db->query($sql);

	while ($customer_row = $customer_data->fetchArray()) {
		$customer_id = $customer_row['id'];
		$customer_username = $customer_row['username'];
		$customer_password = $customer_row['password'];
		$customer_status = $customer_row['status'];
		$customer_expiry = $customer_row['expiry'];
	}

	$sql = 'SELECT * FROM xc_domains_ottrun;';
	$domain_data = $db->query($sql);

	while ($domain_row = $domain_data->fetchArray()) {
		$domain_string = '';

		if ($domain_row['ssl'] == 'true') {
			$domain_string = 'https://';
		}
		else {
			$domain_string = 'http://';
		}

		$domain_string .= $domain_row['dns'];
		$domain_string .= ':' . $domain_row['port'];
		$api_call = $domain_string . '/player_api.php?username=' . $_GET['username'] . '&password=' . $_GET['password'];
		$player_api = @json_decode(file_get_contents($api_call), true);
		$user_info = @json_decode(json_encode($player_api['user_info']), true);
		$customer_auth = @$user_info['auth'];

		if ($customer_auth == 1) {
			echo json_encode($player_api);
			exit();
		}
	}
	if ((@$customer_status == 'Active') && ((time() < strtotime($customer_expiry)) == 1)) {
		if (($_GET['username'] == $customer_username) && ($_GET['password'] == $customer_password)) {
			echo json_response(new UserInfoConfig($customer_username, $customer_password, 1, $customer_status, '' . strtotime($customer_expiry)), new ServerInfoConfig());
		}
	}
	else {
		echo json_response(NULL);
	}
}
else {
	echo json_response(NULL);
}

?>