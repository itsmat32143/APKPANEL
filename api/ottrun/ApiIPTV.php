<?php


class AppConfig
{
	public $id = '1858';
	public $appname = 'XC';
	public $customerid = '521064';
	public $expire = '2099-12-31 23:59:59';
	public $version_code = '722';
	public $login_type = 'login';
	public $portal = 'http://0.0.0.0/';
	public $portal2 = 'http://0.0.0.0/';
	public $portal3 = 'http://0.0.0.0/';
	public $portal4 = 'http://0.0.0.0/';
	public $portal5 = 'http://0.0.0.0/';
	public $portal_name = 'Portal';
	public $portal2_name = 'Portal 2';
	public $portal3_name = 'Portal 3';
	public $portal4_name = 'Portal 4';
	public $portal5_name = 'Portal 5';
	public $portal_vod = 'no';
	public $portal_series = 'no';
	public $apkurl = '';
	public $backupurl = '';
	public $logurl = '';
	public $apkautoupdate = 'no';
	public $support_email = 'Please contact:';
	public $support_phone = 'Support Group';
	public $status = 'ACTIVE';
	public $filter_status = 'No';
	public $epg_mode = 'No';
	public $btn_live = 'No';
	public $btn_live2 = 'No';
	public $btn_live3 = 'No';
	public $btn_live4 = 'No';
	public $btn_live5 = 'No';
	public $btn_vod = 'No';
	public $btn_vod2 = 'No';
	public $btn_vod3 = 'No';
	public $btn_vod4 = 'No';
	public $btn_vod5 = 'No';
	public $btn_epg = 'No';
	public $btn_epg2 = 'No';
	public $btn_epg3 = 'No';
	public $btn_epg4 = 'No';
	public $btn_epg5 = 'No';
	public $btn_series = 'No';
	public $btn_series2 = 'No';
	public $btn_series3 = 'No';
	public $btn_series4 = 'No';
	public $btn_series5 = 'No';
	public $btn_radio = 'No';
	public $btn_radio2 = 'No';
	public $btn_radio3 = 'No';
	public $btn_radio4 = 'No';
	public $btn_radio5 = 'No';
	public $btn_catchup = 'No';
	public $btn_catchup2 = 'No';
	public $btn_catchup3 = 'No';
	public $btn_catchup4 = 'No';
	public $btn_catchup5 = 'No';
	public $btn_account = 'no';
	public $btn_account2 = 'no';
	public $btn_account3 = 'no';
	public $btn_account4 = 'no';
	public $btn_account5 = 'no';
	public $btn_pr = 'no';
	public $btn_rec = 'no';
	public $btn_vpn = 'no';
	public $btn_noti = 'no';
	public $btn_update = 'no';
	public $btn_login_settings = 'no';
	public $btn_login_account = 'no';
	public $show_expire = 'no';
	public $agent = 'XCIPTV';
	public $all_cat = 'no';
	public $stream_type = 'm3u8';
	public $player = 'EXO';
	public $player_tv = 'EXO';
	public $player_vod = 'VLC';
	public $player_series = 'VLC';
	public $player_catchup = 'VLC';
	public $whichplayer = 'EXO';
	public $whichplayer_tv = 'EXO';
	public $whichplayer_vod = 'EXO';
	public $whichplayer_series = 'EXO';
	public $whichplayer_catchup = 'EXO';
	public $message_enabled = 'no';
	public $announcement_enabled = 'no';
	public $updateuserinfo_enabled = 'no';
	public $whatsupcheck_enabled = 'no';
	public $login_logo = 'no';
	public $ms = 'no';
	public $ms2 = 'no';
	public $ms3 = 'no';
	public $btn_fav = 'no';
	public $btn_fav2 = 'no';
	public $btn_fav3 = 'no';
	public $btn_signup = 'no';
	public $bjob = 'no';
	public $settings_app = 'no';
	public $settings_account = 'no';
	public $logs = 'no';
	public $panel = 'xtreamcodes';
	public $epg_url = 'no';
	public $ovpn_url = 'no';
	public $app_language = 'en';
	public $load_last_channel = 'no';
	public $admob_banner_id = 'no';
	public $admob_interstitial_id = 'no';
	public $show_cat_count = 'no';
	public $activation_url = 'no';
	public $send_udid = 'no';
	public $theme = 'd';
	public $ort_settings = '';
	public $vastconfig;
	public $admobconfig;
	public $mnt_message = 'Maintenance Message';
	public $mnt_status = 'INACTIVE';
	public $mnt_expire = '2099-12-31 23:59:59';

	public function __construct($id, $appname, $customerid, $expire, $version_code, $login_type, $portal, $portal2, $portal3, $portal4, $portal5, $portal_name, $portal2_name, $portal3_name, $portal4_name, $portal5_name, $portal_vod, $portal_series, $apkurl, $backupurl, $logurl, $apkautoupdate, $support_email, $support_phone, $status, $filter_status, $epg_mode, $btn_live, $btn_live2, $btn_live3, $btn_live4, $btn_live5, $btn_vod, $btn_vod2, $btn_vod3, $btn_vod4, $btn_vod5, $btn_epg, $btn_epg2, $btn_epg3, $btn_epg4, $btn_epg5, $btn_series, $btn_series2, $btn_series3, $btn_series4, $btn_series5, $btn_radio, $btn_radio2, $btn_radio3, $btn_radio4, $btn_radio5, $btn_catchup, $btn_catchup2, $btn_catchup3, $btn_catchup4, $btn_catchup5, $btn_account, $btn_account2, $btn_account3, $btn_account4, $btn_account5, $btn_pr, $btn_rec, $btn_vpn, $btn_noti, $btn_update, $btn_login_settings, $btn_login_account, $show_expire, $agent, $all_cat, $stream_type, $player, $player_tv, $player_vod, $player_series, $player_catchup, $whichplayer, $whichplayer_tv, $whichplayer_vod, $whichplayer_series, $whichplayer_catchup, $message_enabled, $announcement_enabled, $updateuserinfo_enabled, $whatsupcheck_enabled, $login_logo, $ms, $ms2, $ms3, $btn_fav, $btn_fav2, $btn_fav3, $btn_signup, $bjob, $settings_app, $settings_account, $logs, $panel, $epg_url, $ovpn_url, $app_language, $load_last_channel, $admob_banner_id, $admob_interstitial_id, $show_cat_count, $activation_url, $send_udid, $theme, $ort_settings, $vastconfig, $admobconfig, $mnt_message, $mnt_status, $mnt_expire)
	{
		$this->id = $id;
		$this->appname = $appname;
		$this->customerid = $customerid;
		$this->expire = $expire;
		$this->version_code = $version_code;
		$this->login_type = $login_type;
		$this->portal = $portal;
		$this->portal2 = $portal2;
		$this->portal3 = $portal3;
		$this->portal4 = $portal4;
		$this->portal5 = $portal5;
		$this->portal_name = $portal_name;
		$this->portal2_name = $portal2_name;
		$this->portal3_name = $portal3_name;
		$this->portal4_name = $portal4_name;
		$this->portal5_name = $portal5_name;
		$this->portal_vod = $portal_vod;
		$this->portal_series = $portal_series;
		$this->apkurl = $apkurl;
		$this->backupurl = $backupurl;
		$this->logurl = $logurl;
		$this->apkautoupdate = $apkautoupdate;
		$this->support_email = $support_email;
		$this->support_phone = $support_phone;
		$this->status = $status;
		$this->filter_status = $filter_status;
		$this->epg_mode = $epg_mode;
		$this->btn_live = $btn_live;
		$this->btn_live2 = $btn_live2;
		$this->btn_live3 = $btn_live3;
		$this->btn_live4 = $btn_live4;
		$this->btn_live5 = $btn_live5;
		$this->btn_vod = $btn_vod;
		$this->btn_vod2 = $btn_vod2;
		$this->btn_vod3 = $btn_vod3;
		$this->btn_vod4 = $btn_vod4;
		$this->btn_vod5 = $btn_vod5;
		$this->btn_epg = $btn_epg;
		$this->btn_epg2 = $btn_epg2;
		$this->btn_epg3 = $btn_epg3;
		$this->btn_epg4 = $btn_epg4;
		$this->btn_epg5 = $btn_epg5;
		$this->btn_series = $btn_series;
		$this->btn_series2 = $btn_series2;
		$this->btn_series3 = $btn_series3;
		$this->btn_series4 = $btn_series4;
		$this->btn_series5 = $btn_series5;
		$this->btn_radio = $btn_radio;
		$this->btn_radio2 = $btn_radio2;
		$this->btn_radio3 = $btn_radio3;
		$this->btn_radio4 = $btn_radio4;
		$this->btn_radio5 = $btn_radio5;
		$this->btn_catchup = $btn_catchup;
		$this->btn_catchup2 = $btn_catchup2;
		$this->btn_catchup3 = $btn_catchup3;
		$this->btn_catchup4 = $btn_catchup4;
		$this->btn_catchup5 = $btn_catchup5;
		$this->btn_account = $btn_account;
		$this->btn_account2 = $btn_account2;
		$this->btn_account3 = $btn_account3;
		$this->btn_account4 = $btn_account4;
		$this->btn_account5 = $btn_account5;
		$this->btn_pr = $btn_pr;
		$this->btn_rec = $btn_rec;
		$this->btn_vpn = $btn_vpn;
		$this->btn_noti = $btn_noti;
		$this->btn_update = $btn_update;
		$this->btn_login_settings = $btn_login_settings;
		$this->btn_login_account = $btn_login_account;
		$this->show_expire = $show_expire;
		$this->agent = $agent;
		$this->all_cat = $all_cat;
		$this->stream_type = $stream_type;
		$this->player = $player;
		$this->player_tv = $player_tv;
		$this->player_vod = $player_vod;
		$this->player_series = $player_series;
		$this->player_catchup = $player_catchup;
		$this->{$whichplayer} = $whichplayer;
		$this->{$whichplayer_tv} = $whichplayer_tv;
		$this->{$whichplayer_vod} = $whichplayer_vod;
		$this->{$whichplayer_series} = $whichplayer_series;
		$this->{$whichplayer_catchup} = $whichplayer_catchup;
		$this->message_enabled = $message_enabled;
		$this->announcement_enabled = $announcement_enabled;
		$this->updateuserinfo_enabled = $updateuserinfo_enabled;
		$this->whatsupcheck_enabled = $whatsupcheck_enabled;
		$this->login_logo = $login_logo;
		$this->ms = $ms;
		$this->ms2 = $ms2;
		$this->ms3 = $ms3;
		$this->btn_fav = $btn_fav;
		$this->btn_fav2 = $btn_fav2;
		$this->btn_fav3 = $btn_fav3;
		$this->btn_signup = $btn_signup;
		$this->bjob = $bjob;
		$this->settings_app = $settings_app;
		$this->settings_account = $settings_account;
		$this->logs = $logs;
		$this->panel = $panel;
		$this->epg_url = $epg_url;
		$this->ovpn_url = $ovpn_url;
		$this->app_language = $app_language;
		$this->load_last_channel = $load_last_channel;
		$this->admob_banner_id = $admob_banner_id;
		$this->admob_interstitial_id = $admob_interstitial_id;
		$this->show_cat_count = $show_cat_count;
		$this->activation_url = $activation_url;
		$this->send_udid = $send_udid;
		$this->theme = $theme;
		$this->ort_settings = $ort_settings;
		$this->vastconfig = $vastconfig;
		$this->admobconfig = $admobconfig;
		$this->mnt_message = $mnt_message;
		$this->mnt_status = $mnt_status;
		$this->mnt_expire = $mnt_expire;
	}
}

class VPNConfig
{
	public $id = '0';
	public $userid = '521064';
	public $vpn_appid = '1646';
	public $vpn_country = 'UK';
	public $vpn_state = 'Liverpool';
	public $vpn_config = 'http://0.0.0.0/';
	public $vpn_status = 'INACTIVE';
	public $auth_type = 'up';
	public $auth_embedded = 'NO';
	public $username = 'Ian';
	public $password = 'fuckyourself(:';
	public $date = '2099-12-31';

	public function __construct($id, $vpn_country, $vpn_state, $vpn_config, $vpn_status, $auth_type, $auth_embedded, $username, $password)
	{
		$this->id = $id;
		$this->vpn_country = $vpn_country;
		$this->vpn_state = $vpn_state;
		$this->vpn_config = $vpn_config;
		$this->vpn_status = $vpn_status;
		$this->auth_type = $auth_type;
		$this->auth_embedded = $auth_embedded;
		$this->username = $username;
		$this->password = $password;
	}
}

function checkupdate_response()
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_xciptv_app;');
	$xc_xciptv_app_data = $result->fetchArray();
	$response['success'] = '1';
	$response['version_code'] = $xc_xciptv_app_data['version_code'];
	$response['apkurl'] = $xc_xciptv_app_data['apkurl'];
	return json_encode($response);
}

function connv2_response($appid, $version, $device_type, $p, $an, $customerid, $userid, $online, $did)
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$sql = 'SELECT * FROM ottrun_messages;';
	$result = $db->query($sql);

	while ($data = $result->fetchArray()) {
		if ($userid == $data['username']) {
			$username = $data['username'];
			$expiration = $data['expiration'];
			$message = $data['message'];
		}
	}

	$result = $db->query('SELECT * FROM xc_xciptv_ann;');
	$xciptv_ann_data = $result->fetchArray();

	if ($userid == @$username) {
		return json_encode(['tag' => 'connv2', 'success' => '1', 'api_ver' => '1.0v', 'message' => $message, 'msgid' => '1646', 'msg_status' => 'ACTIVE', 'msg_expire' => $expiration, 'announcement' => $xciptv_ann_data['announcement'], 'ann_status' => $xciptv_ann_data['status'], 'ann_expire' => $xciptv_ann_data['expire'], 'ann_interval' => $xciptv_ann_data['interval'], 'ann_disappear' => $xciptv_ann_data['disappear']]);
	}
	else {
		return json_encode(['tag' => 'connv2', 'success' => '1', 'api_ver' => '1.0v', 'message' => '', 'msgid' => '1646', 'msg_status' => 'INACTIVE', 'msg_expire' => date('Y-m-d h:i:s'), 'announcement' => '', 'ann_status' => 'INACTIVE', 'ann_expire' => date('Y-m-d h:i:s'), 'ann_interval' => '', 'ann_disappear' => '']);
	}
}

function licV3_response($message = NULL)
{
	return json_encode(['tag' => 'licV3', 'success' => '1', 'api_ver' => '2.0v', 'which' => 'licV3', 'app' => $message]);
}

function vpnconfigV2_response($message = NULL)
{
	return json_encode(['tag' => 'vpnconfigV2', 'success' => '1', 'api_ver' => '1.0v', 'vpnconfigs' => $message]);
}

function announcement_response($announcement = NULL, $status = NULL, $channel = NULL, $expire = NULL, $interval = NULL, $disappear = NULL)
{
	return json_encode(['tag' => 'ann', 'success' => '1', 'api_ver' => '1.0v', 'announcement' => $announcement, 'status' => $status, 'channel' => $channel, 'expire' => $expire, 'interval' => $interval, 'disappear' => $disappear]);
}

function app_array()
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_xciptv_app;');
	$xciptv_app_settings = $result->fetchArray();
	$result = $db->query('SELECT * FROM xc_xciptv_lic;');
	$xciptv_lic = $result->fetchArray();
	return ['id' => $xciptv_app_settings['id'], 'appname' => $xciptv_lic['appname'], 'expire' => 'LIFETIME', 'login_type' => $xciptv_app_settings['login_type'], 'version_code' => $xciptv_app_settings['version_code'], 'filter_status' => $xciptv_app_settings['filter_status'], 'epg_mode' => $xciptv_app_settings['epg_mode'], 'apkautoupdate' => $xciptv_app_settings['apkautoupdate'], 'show_expire' => $xciptv_app_settings['show_expire']];
}

function portal_array()
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_xciptv_app;');
	$xciptv_app_settings = $result->fetchArray();
	return ['panel' => $xciptv_app_settings['panel'], 'portal' => $xciptv_app_settings['portal'], 'portal2' => $xciptv_app_settings['portal2'], 'portal3' => $xciptv_app_settings['portal3'], 'portal4' => $xciptv_app_settings['portal4'], 'portal5' => $xciptv_app_settings['portal5'], 'portal_name' => $xciptv_app_settings['portal_name'], 'portal2_name' => $xciptv_app_settings['portal2_name'], 'portal3_name' => $xciptv_app_settings['portal3_name'], 'portal4_name' => $xciptv_app_settings['portal4_name'], 'portal5_name' => $xciptv_app_settings['portal5_name'], 'portal_vod' => $xciptv_app_settings['portal_vod'], 'portal_series' => $xciptv_app_settings['portal_series']];
}

function urls_array()
{
	$here_be_pirates = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] !== 'off') ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_xciptv_app;');
	$xciptv_app_settings = $result->fetchArray();
	return ['apkurl' => $xciptv_app_settings['apkurl'], 'backupurl' => $here_be_pirates, 'logurl' => $here_be_pirates, 'activation_url' => $xciptv_app_settings['activation_url'], 'socket_url' => $here_be_pirates, 'epg_url' => $xciptv_app_settings['epg_url'], 'ovpn_url' => $xciptv_app_settings['ovpn_url']];
}

function support_array()
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_xciptv_app;');
	$xciptv_app_settings = $result->fetchArray();
	return ['support_email' => $xciptv_app_settings['support_email'], 'support_phone' => $xciptv_app_settings['support_phone']];
}

function button_array()
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_xciptv_app;');
	$xciptv_app_settings = $result->fetchArray();
	return ['btn_live' => $xciptv_app_settings['btn_live'], 'btn_live2' => $xciptv_app_settings['btn_live2'], 'btn_live3' => $xciptv_app_settings['btn_live3'], 'btn_live4' => $xciptv_app_settings['btn_live4'], 'btn_live5' => $xciptv_app_settings['btn_live5'], 'btn_vod' => $xciptv_app_settings['btn_vod'], 'btn_vod2' => $xciptv_app_settings['btn_vod2'], 'btn_vod3' => $xciptv_app_settings['btn_vod3'], 'btn_vod4' => $xciptv_app_settings['btn_vod4'], 'btn_vod5' => $xciptv_app_settings['btn_vod5'], 'btn_epg' => $xciptv_app_settings['btn_epg'], 'btn_epg2' => $xciptv_app_settings['btn_epg2'], 'btn_epg3' => $xciptv_app_settings['btn_epg3'], 'btn_epg4' => $xciptv_app_settings['btn_epg4'], 'btn_epg5' => $xciptv_app_settings['btn_epg5'], 'btn_series' => $xciptv_app_settings['btn_series'], 'btn_series2' => $xciptv_app_settings['btn_series2'], 'btn_series3' => $xciptv_app_settings['btn_series3'], 'btn_series4' => $xciptv_app_settings['btn_series4'], 'btn_series5' => $xciptv_app_settings['btn_series5'], 'btn_radio' => $xciptv_app_settings['btn_radio'], 'btn_radio2' => $xciptv_app_settings['btn_radio2'], 'btn_radio3' => $xciptv_app_settings['btn_radio3'], 'btn_radio4' => $xciptv_app_settings['btn_radio4'], 'btn_radio5' => $xciptv_app_settings['btn_radio5'], 'btn_catchup' => $xciptv_app_settings['btn_catchup'], 'btn_catchup2' => $xciptv_app_settings['btn_catchup2'], 'btn_catchup3' => $xciptv_app_settings['btn_catchup3'], 'btn_catchup4' => $xciptv_app_settings['btn_catchup4'], 'btn_catchup5' => $xciptv_app_settings['btn_catchup5'], 'btn_account' => $xciptv_app_settings['btn_account'], 'btn_account2' => $xciptv_app_settings['btn_account2'], 'btn_account3' => $xciptv_app_settings['btn_account3'], 'btn_pr' => $xciptv_app_settings['btn_pr'], 'btn_rec' => $xciptv_app_settings['btn_rec'], 'btn_vpn' => $xciptv_app_settings['btn_vpn'], 'btn_noti' => $xciptv_app_settings['btn_noti'], 'btn_update' => $xciptv_app_settings['btn_update'], 'btn_login_settings' => $xciptv_app_settings['btn_login_setting'], 'btn_login_account' => $xciptv_app_settings['btn_login_account'], 'btn_fav' => $xciptv_app_settings['btn_fav'], 'btn_fav2' => $xciptv_app_settings['btn_fav2'], 'btn_fav3' => $xciptv_app_settings['btn_fav3'], 'btn_signup' => $xciptv_app_settings['btn_signup'], 'ms' => $xciptv_app_settings['ms'], 'ms2' => $xciptv_app_settings['ms2'], 'ms3' => $xciptv_app_settings['ms3']];
}

function settings_array()
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_xciptv_app;');
	$xciptv_app_settings = $result->fetchArray();
	return ['agent' => $xciptv_app_settings['agent'], 'all_cat' => 'yes', 'message_enabled' => $xciptv_app_settings['message_enable'], 'announcement_enabled' => $xciptv_app_settings['announcement_enabled'], 'updateuserinfo_enabled' => $xciptv_app_settings['updateuserinfo_enabled'], 'whatsupcheck_enabled' => $xciptv_app_settings['whatsupcheck_enabled'], 'login_logo' => $xciptv_app_settings['login_logo'], 'bjob' => $xciptv_app_settings['bjob'], 'settings_app' => $xciptv_app_settings['settings_app'], 'settings_account' => $xciptv_app_settings['settings_account'], 'logs' => $xciptv_app_settings['logs'], 'app_language' => $xciptv_app_settings['app_language'], 'load_last_channel' => $xciptv_app_settings['load_last_channel'], 'admob_banner_id' => $xciptv_app_settings['admob_banner_id'], 'admob_interstitial_id' => $xciptv_app_settings['admob_interstitial_id'], 'show_cat_count' => 'yes', 'send_udid' => $xciptv_app_settings['send_udid'], 'theme' => $xciptv_app_settings['theme'], 'vpn_login_view' => 'yes'];
}

function players_array()
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_xciptv_app;');
	$xciptv_app_settings = $result->fetchArray();
	return ['player' => $xciptv_app_settings['player'], 'player_tv' => $xciptv_app_settings['player_tv'], 'player_vod' => $xciptv_app_settings['player_vod'], 'player_series' => $xciptv_app_settings['player_series'], 'player_catchup' => $xciptv_app_settings['player_catchup'], 'stream_type' => $xciptv_app_settings['stream_type']];
}

function ortsettings_array()
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_ort_settings;');
	$xciptv_ort_settings = $result->fetchArray();
	return ['vlc_hw' => $xciptv_ort_settings['vlc_hw'], 'last_volume_vlc' => $xciptv_ort_settings['last_volume_vlc'], 'plyer_vlc_buffer' => $xciptv_ort_settings['player_vlc_buffer'], 'video_resize_vlc' => $xciptv_ort_settings['video_resize_vlc'], 'video_subtiltes_vlc' => $xciptv_ort_settings['video_subtitles_vlc'], 'exo_hw' => $xciptv_ort_settings['exo_hw'], 'last_volume_exo' => $xciptv_ort_settings['last_volume_exo'], 'plyer_exo_buffer' => $xciptv_ort_settings['player_exo_buffer'], 'video_resize_exo' => $xciptv_ort_settings['video_resize_exo'], 'video_subtiltes_exo' => $xciptv_ort_settings['video_subtitles_exo']];
}

function maintenance_array()
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_xciptv_app;');
	$xciptv_app_settings = $result->fetchArray();
	return ['mnt_message' => $xciptv_app_settings['mnt_message'], 'mnt_status' => $xciptv_app_settings['mnt_status'], 'mnt_expire' => $xciptv_app_settings['mnt_expire']];
}

function admob_array()
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_xciptv_ads;');
	$xciptv_ads_settings = $result->fetchArray();
	return ['admob_enabled' => $xciptv_ads_settings['admob_enabled']];
}

function prebid_array()
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_xciptv_ads;');
	$xciptv_ads_settings = $result->fetchArray();
	return ['prebid_enabled' => $xciptv_ads_settings['prebid_enabled'], 'Host' => $xciptv_ads_settings['prebid_host'], 'AdUnitId' => $xciptv_ads_settings['prebid_adunitid'], 'AccountId' => $xciptv_ads_settings['prebid_accountid'], 'Banner' => $xciptv_ads_settings['prebid_banner']];
}

function vast_array()
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_xciptv_ads;');
	$xciptv_ads_settings = $result->fetchArray();
	return ['vast_enabled' => $xciptv_ads_settings['vast_enabled'], 'mid_roll_interval' => $xciptv_ads_settings['vast_midrollinterval'], 'post_roll_start_at' => $xciptv_ads_settings['vast_postrollstartat'], 'vod_mid_roll_interval' => $xciptv_ads_settings['vast_vodmidrollinterval'], 'vod_pre_roll_url' => $xciptv_ads_settings['vast_vodprerollurl'], 'vod_mid_roll_url' => $xciptv_ads_settings['vast_vodmidrollurl'], 'vod_post_roll_url' => $xciptv_ads_settings['vast_vodpostrollurl'], 'series_mid_roll_interval' => $xciptv_ads_settings['vast_seriesmidrollinterval'], 'series_pre_roll_url' => $xciptv_ads_settings['vast_seriesprerollurl'], 'series_mid_roll_url' => $xciptv_ads_settings['vast_seriesmidrollurl'], 'series_post_roll_url' => $xciptv_ads_settings['vast_seriespostrollurl']];
}

function licV4_response()
{
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM xc_xciptv_lic;');
	$xciptv_lic = $result->fetchArray();
	$result = $db->query('SELECT * FROM xc_xciptv_ads;');
	$xciptv_ads = $result->fetchArray();
	$url = 'https://us-central1-xciptv-licv4.cloudfunctions.net/app/' . str_replace(' ', '%20', '' . $xciptv_lic['appname']) . '/' . $xciptv_lic['cid'];
	$curl = curl_init($url);
	$data = json_encode(['success' => '1', 'status' => 'ACTIVE', 'cid' => $xciptv_lic['cid'], 'which' => 'licV4', 'app' => app_array(), 'portal' => portal_array(), 'urls' => urls_array(), 'support' => support_array(), 'button' => button_array(), 'settings' => settings_array(), 'admobconfig' => admob_array(), 'prebid' => prebid_array(), 'vastconfig' => vast_array(), 'freestar' => $xciptv_ads['freestar_enabled'], 'players' => players_array(), 'ort_settings' => ortsettings_array(), 'maintenance' => maintenance_array(), 'created-by' => 'Ian']);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	curl_setopt($curl, CURLOPT_HTTPHEADER, ['content-type: application/json;charset=utf-8']);
	return curl_exec($curl);
}

error_reporting(E_ALL);
ini_set('display_errors', 1);

if (isset($_REQUEST['tag'])) {
	$db = new SQLite3('../../api/.cockpit-0001.db');
	$result = $db->query('SELECT * FROM ovpn_sorting;');
	$ovpn_sorting = $result->fetchArray();
	$result = $db->query('SELECT * FROM ovpn_config ORDER BY ' . $ovpn_sorting['group_by'] . ' ' . $ovpn_sorting['order_by'] . ';');
	$config_data = $db->query('DROP TABLE ovpn_config_sorted;');
	$config_data = $db->query('CREATE TABLE IF NOT EXISTS ovpn_config_sorted (id INTEGER PRIMARY KEY, vpn_type TEXT, vpn_country TEXT, vpn_location TEXT, vpn_config TEXT, vpn_status TEXT, auth_type TEXT, auth_embedded TEXT);');
	$count = 0;

	while ($config_data_unsorted = $result->fetchArray()) {
		$count++;
		$config_data = $db->query('INSERT INTO ovpn_config_sorted (\'id\', \'vpn_type\', \'vpn_country\', \'vpn_location\', \'vpn_config\', \'vpn_status\', \'auth_type\', \'auth_embedded\') VALUES (' . $count . ', \'' . $config_data_unsorted['vpn_type'] . '\', \'' . $config_data_unsorted['vpn_country'] . '\', \'' . $config_data_unsorted['vpn_location'] . '\', \'' . $config_data_unsorted['vpn_config'] . '\', \'' . $config_data_unsorted['vpn_status'] . '\', \'' . $config_data_unsorted['auth_type'] . '\', \'' . $config_data_unsorted['auth_embedded'] . '\');');
	}

	$config_data = $db->query('SELECT * FROM ovpn_config_sorted;');
	$credentials_data = $db->query('SELECT * FROM ovpn_credentials;');
	$result = $db->query('SELECT * FROM xc_xciptv_ann;');
	$xciptv_ann_data = $result->fetchArray();
	$result = $db->query('SELECT * FROM xc_xciptv_app;');
	$xciptv_app_data = $result->fetchArray();
	$result = $db->query('SELECT * FROM xc_ort_settings;');
	$xciptv_ort_settings = $result->fetchArray();
	$result = $db->query('SELECT * from xc_xciptv_lic;');
	$xciptv_lic = $result->fetchArray();
	$vpnconfigs_arr = [];
	$appconfigs_arr = [];

	if ($_GET['tag'] === 'vpnconfigV2') {
		while ($config_row = $config_data->fetchArray()) {
			if ($config_row['auth_type'] != 'noup') {
				while ($credentials_row = $credentials_data->fetchArray()) {
					if ($config_row['vpn_type'] == 'Surfshark') {
						array_push($vpnconfigs_arr, new VPNConfig('' . $config_row['id'], $config_row['vpn_country'], $config_row['vpn_location'], $config_row['vpn_config'], $config_row['vpn_status'], $config_row['auth_type'], $config_row['auth_embedded'], $credentials_row['surfshark_username'], $credentials_row['surfshark_password']));
						continue;
					}

					if ($config_row['vpn_type'] == 'IPVanish') {
						array_push($vpnconfigs_arr, new VPNConfig('' . $config_row['id'], $config_row['vpn_country'], $config_row['vpn_location'], $config_row['vpn_config'], $config_row['vpn_status'], $config_row['auth_type'], $config_row['auth_embedded'], $credentials_row['ipvanish_username'], $credentials_row['ipvanish_password']));
						continue;
					}

					if ($config_row['vpn_type'] == 'NordVPN') {
						array_push($vpnconfigs_arr, new VPNConfig('' . $config_row['id'], $config_row['vpn_country'], $config_row['vpn_location'], $config_row['vpn_config'], $config_row['vpn_status'], $config_row['auth_type'], $config_row['auth_embedded'], $credentials_row['nordvpn_username'], $credentials_row['nordvpn_password']));
						continue;
					}

					if ($config_row['vpn_type'] == 'OpenVPN') {
						array_push($vpnconfigs_arr, new VPNConfig('' . $config_row['id'], $config_row['vpn_country'], $config_row['vpn_location'], $config_row['vpn_config'], $config_row['vpn_status'], $config_row['auth_type'], $config_row['auth_embedded'], $credentials_row['ovpn_username'], $credentials_row['ovpn_password']));
					}
				}

				continue;
			}

			array_push($vpnconfigs_arr, new VPNConfig('' . $config_row['id'], $config_row['vpn_country'], $config_row['vpn_location'], $config_row['vpn_config'], $config_row['vpn_status'], $config_row['auth_type'], $config_row['auth_embedded'], '', ''));
		}

		echo bin2hex(openssl_encrypt(vpnconfigV2_response($vpnconfigs_arr), 'AES-128-CBC', 'mysecretkeywsdef', 1, 'myuniqueivparamu'));
	}

	if ($_GET['tag'] === 'licV3') {
		echo bin2hex(openssl_encrypt(licV3_response(new AppConfig('' . $xciptv_app_data['id'], $xciptv_app_data['appname'], '' . $xciptv_app_data['customerid'], $xciptv_app_data['expire'], '' . $xciptv_app_data['version_code'], $xciptv_app_data['login_type'], '' . $xciptv_app_data['portal'], '' . $xciptv_app_data['portal2'], '' . $xciptv_app_data['portal3'], '' . $xciptv_app_data['portal4'], '' . $xciptv_app_data['portal5'], $xciptv_app_data['portal_name'], $xciptv_app_data['portal2_name'], $xciptv_app_data['portal3_name'], $xciptv_app_data['portal4_name'], $xciptv_app_data['portal5_name'], $xciptv_app_data['portal_vod'], $xciptv_app_data['portal_series'], $xciptv_app_data['apkurl'], (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] !== 'off') ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/', (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] !== 'off') ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/', $xciptv_app_data['apkautoupdate'], $xciptv_app_data['support_email'], $xciptv_app_data['support_phone'], $xciptv_app_data['status'], $xciptv_app_data['filter_status'], $xciptv_app_data['epg_mode'], $xciptv_app_data['btn_live'], $xciptv_app_data['btn_live2'], $xciptv_app_data['btn_live3'], $xciptv_app_data['btn_live4'], $xciptv_app_data['btn_live5'], $xciptv_app_data['btn_vod'], $xciptv_app_data['btn_vod2'], $xciptv_app_data['btn_vod3'], $xciptv_app_data['btn_vod4'], $xciptv_app_data['btn_vod5'], $xciptv_app_data['btn_epg'], $xciptv_app_data['btn_epg2'], $xciptv_app_data['btn_epg3'], $xciptv_app_data['btn_epg4'], $xciptv_app_data['btn_epg5'], $xciptv_app_data['btn_series'], $xciptv_app_data['btn_series2'], $xciptv_app_data['btn_series3'], $xciptv_app_data['btn_series4'], $xciptv_app_data['btn_series5'], $xciptv_app_data['btn_radio'], $xciptv_app_data['btn_radio2'], $xciptv_app_data['btn_radio3'], $xciptv_app_data['btn_radio4'], $xciptv_app_data['btn_radio5'], $xciptv_app_data['btn_catchup'], $xciptv_app_data['btn_catchup2'], $xciptv_app_data['btn_catchup3'], $xciptv_app_data['btn_catchup4'], $xciptv_app_data['btn_catchup5'], $xciptv_app_data['btn_account'], $xciptv_app_data['btn_account2'], $xciptv_app_data['btn_account3'], $xciptv_app_data['btn_account4'], $xciptv_app_data['btn_account5'], $xciptv_app_data['btn_pr'], $xciptv_app_data['btn_rec'], $xciptv_app_data['btn_vpn'], $xciptv_app_data['btn_noti'], $xciptv_app_data['btn_update'], $xciptv_app_data['btn_login_setting'], $xciptv_app_data['btn_login_account'], $xciptv_app_data['show_expire'], $xciptv_app_data['agent'], $xciptv_app_data['all_cat'], $xciptv_app_data['stream_type'], $xciptv_app_data['player'], $xciptv_app_data['player_tv'], $xciptv_app_data['player_vod'], $xciptv_app_data['player_series'], $xciptv_app_data['player_catchup'], $xciptv_app_data['player'], $xciptv_app_data['player_tv'], $xciptv_app_data['player_vod'], $xciptv_app_data['player_series'], $xciptv_app_data['player_catchup'], $xciptv_app_data['message_enable'], $xciptv_app_data['announcement_enabled'], $xciptv_app_data['updateuserinfo_enabled'], $xciptv_app_data['whatsupcheck_enabled'], $xciptv_app_data['login_logo'], $xciptv_app_data['ms'], $xciptv_app_data['ms2'], $xciptv_app_data['ms3'], $xciptv_app_data['btn_fav'], $xciptv_app_data['btn_fav2'], $xciptv_app_data['btn_fav3'], $xciptv_app_data['btn_signup'], $xciptv_app_data['bjob'], $xciptv_app_data['settings_app'], $xciptv_app_data['settings_account'], $xciptv_app_data['logs'], $xciptv_app_data['panel'], $xciptv_app_data['epg_url'], $xciptv_app_data['ovpn_url'], $xciptv_app_data['app_language'], $xciptv_app_data['load_last_channel'], $xciptv_app_data['admob_banner_id'], $xciptv_app_data['admob_interstitial_id'], $xciptv_app_data['show_cat_count'], $xciptv_app_data['activation_url'], $xciptv_app_data['send_udid'], '' . $xciptv_app_data['theme'], ortsettings_array(), NULL, NULL, $xciptv_app_data['mnt_message'], $xciptv_app_data['mnt_status'], $xciptv_app_data['mnt_expire'])), 'AES-128-CBC', 'mysecretkeywsdef', 1, 'myuniqueivparamu'));
	}

	if ($_GET['tag'] === 'licV4') {
		echo licV4_response();
	}

	if ($_GET['tag'] == 'ann') {
		echo announcement_response($xciptv_ann_data['announcement'], $xciptv_ann_data['status'], $xciptv_ann_data['channel'], $xciptv_ann_data['expire'], $xciptv_ann_data['interval'], $xciptv_ann_data['disappear']);
	}

	if ($_GET['tag'] == 'connv2') {
		$sql = 'REPLACE INTO xc_xciptv_connv2(appid, version, device_type, p, an, customerid, userid, online, did, datetime) VALUES (\'' . $_GET['appid'] . '\', \'' . $_GET['version'] . '\', \'' . $_GET['device_type'] . '\', \'' . $_GET['p'] . '\', \'' . $_GET['an'] . '\', \'' . $_GET['customerid'] . '\', \'' . $_GET['userid'] . '\', \'' . $_GET['online'] . '\', \'' . $_GET['did'] . '\', \'' . date('Y-m-d h:i:s') . '\');';
		$db->exec($sql);
		echo connv2_response('' . $_GET['appid'], '' . $_GET['version'], '' . $_GET['device_type'], '' . $_GET['p'], '' . $_GET['an'], '' . $_GET['customerid'], '' . $_GET['userid'], '' . $_GET['online'], '' . $_GET['did']);
	}

	if ($_GET['tag'] == 'checkupdate') {
		echo checkupdate_response();
	}
}
else {
	include 'index.php';
}

?>