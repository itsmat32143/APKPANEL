Thanks for using Cockpit Panel!

Here are some things you may need to know when using the panel.

You can alter some configuration of the panel such as:-
 - Enable/Disable Dashboard
 - Edit Dashboard Articles
 - Enable/Disable Branding Removal
 - Enable/Disable User profile editing
 - Edit Default profile
By editing the 'config.php' file in /assets/

When using the OTTRun panel to control an XCIPTV/ORPlayer application using the LicenceV4 API Scheme you need to ensure that the
Appname (see the applications /res/Strings.xml) and the CustomerID match the application you are connecting with.