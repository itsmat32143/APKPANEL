<?php


session_start();
include 'assets/includes/db.php';
include 'assets/includes/config.php';

if ($_ERRORS) {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}
if (!isset($_SESSION['loggedin']) && !$_SESSION['loggedin']) {
	header('location: logout.php');
}

if (isset($_POST['submit_theme'])) {
	$sql = 'UPDATE xc_xciptv_app SET ';
	if (!isset($_POST['theme-api']) && ($_POST['theme'] != 'new_layout')) {
		$sql .= 'theme = \'' . substr($_POST['theme'], -1) . '\';';
	}
	else {
		$sql .= 'theme = \'' . $_POST['theme'] . '\';';
	}

	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_customization.php');
}

if (isset($_POST['submit_exo'])) {
	$sql = 'UPDATE xc_ort_settings SET ';
	$sql .= 'exo_hw = \'' . $_POST['exo_hw'] . '\', ';
	$sql .= 'last_volume_exo = \'' . $_POST['last_volume_exo'] . '\', ';
	$sql .= 'player_exo_buffer = \'' . $_POST['player_exo_buffer'] . '\', ';
	$sql .= 'video_resize_exo = \'' . $_POST['video_resize_exo'] . '\', ';
	$sql .= 'video_subtitles_exo = \'' . $_POST['video_subtitles_exo'] . '\';';
	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_customization.php');
}

if (isset($_POST['submit_vlc'])) {
	$sql = 'UPDATE xc_ort_settings SET ';
	$sql .= 'vlc_hw = \'' . $_POST['vlc_hw'] . '\', ';
	$sql .= 'last_volume_vlc = \'' . $_POST['last_volume_vlc'] . '\', ';
	$sql .= 'player_vlc_buffer = \'' . $_POST['player_vlc_buffer'] . '\', ';
	$sql .= 'video_resize_vlc = \'' . $_POST['video_resize_vlc'] . '\', ';
	$sql .= 'video_subtitles_vlc = \'' . $_POST['video_subtitles_vlc'] . '\';';
	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_customization.php');
}

if (isset($_POST['submit_intro'])) {
	move_uploaded_file($_FILES['intro']['tmp_name'], './api/ottrun/intro.mp4');
	header('Location: ottrunxciptv_customization.php');
}

if (isset($_POST['submit_tvsg'])) {
	$sql = 'UPDATE tvsg_settings SET ';
	$sql .= 'widget_id = \'' . $_POST['widget_id'] . '\', ';
	$sql .= 'border_color = \'' . $_POST['border_color'] . '\', ';
	$sql .= 'background_color = \'' . $_POST['background_color'] . '\', ';
	$sql .= 'text_color = \'' . $_POST['text_color'] . '\';';
	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_customization.php');
}

echo '<!doctype html>' . "\r\n" . '<html lang="en">' . "\r\n\r\n" . '<head>' . "\r\n\r\n" . '    ';
include 'assets/includes/title-meta.php';
echo "\r\n" . '    ';
include 'assets/includes/head-css.php';
echo "\r\n" . '</head>' . "\r\n\r\n" . '<body data-sidebar="dark">' . "\r\n\r\n" . '    <!-- Loader -->' . "\r\n" . '    <div id="preloader">' . "\r\n" . '        <div id="status">' . "\r\n" . '            <div class="spinner">' . "\r\n" . '                <i class="ri-loader-line spin-icon"></i>' . "\r\n" . '            </div>' . "\r\n" . '        </div>' . "\r\n" . '    </div>' . "\r\n\r\n" . '    <div id="layout-wrapper">' . "\r\n\r\n" . '        ';
include 'assets/includes/topbar.php';
echo "\r\n" . '        ';
include 'assets/includes/sidebar.php';
echo "\r\n" . '        <div class="main-content">' . "\r\n\r\n" . '            <div class="page-content">' . "\r\n" . '                <div class="container-fluid">' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="page-title-box d-flex align-items-center justify-content-between">' . "\r\n" . '                                <h4 class="mb-0"> </h4>' . "\r\n\r\n" . '                                <div class="page-title-right">' . "\r\n" . '                                    <ol class="breadcrumb m-0">' . "\r\n" . '                                        <li class="breadcrumb-item"><a href="javascript: void(0);">OTTRun XCIPTV</a></li>' . "\r\n" . '                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Configuration</a></li>' . "\r\n" . '                                        <li class="breadcrumb-item active">Customization</li>' . "\r\n" . '                                    </ol>' . "\r\n" . '                                </div>' . "\r\n\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <h4 class="card-title">Themes</h4>' . "\r\n" . '                                    <p class="card-title-desc">The selected layout will affect all connected applicatons.</br>Not all Themes work on all versions.</br>Not all Apps support Theme selection.</p>' . "\r\n\r\n" . '                                    <form method="POST">' . "\r\n" . '                                        <div class="form-group col-4 mx-auto">' . "\r\n" . '                                            <div class="square-switch">' . "\r\n" . '                                                <select class="custom-select" id="theme" name="theme">' . "\r\n" . '                                                    <option value="theme_d" ';
if (($xciptv_app_data['theme'] == 'theme_d') || ($xciptv_app_data['theme'] == 'd')) {
	echo 'selected';
}

echo '>Theme 1</option>' . "\r\n" . '                                                    ' . "\r\n" . '                                                    <option value="theme_1" ';
if (($xciptv_app_data['theme'] == 'theme_1') || ($xciptv_app_data['theme'] == '1')) {
	echo 'selected';
}

echo '>Theme 2</option>' . "\r\n" . '                                                    <option value="theme_2" ';
if (($xciptv_app_data['theme'] == 'theme_2') || ($xciptv_app_data['theme'] == '2')) {
	echo 'selected';
}

echo '>Theme 3</option>' . "\r\n" . '                                                    <option value="theme_3" ';
if (($xciptv_app_data['theme'] == 'theme_3') || ($xciptv_app_data['theme'] == '3')) {
	echo 'selected';
}

echo '>Theme 4</option>' . "\r\n" . '                                                    ' . "\r\n" . '                                                </select>' . "\r\n\r\n" . '                                            </div>' . "\r\n" . '                                            <div class="form-group">' . "\r\n" . '                                            <br>    <label for="theme-api">Old API? <small>e.g. \'theme_d\' over \'d\'</small></label>' . "\r\n" . '                                                <div class="square-switch float-right">' . "\r\n" . '                                                    <input type="checkbox" id="theme-api" name="theme-api" switch="bool" ';
echo strlen($xciptv_app_data['theme']) == 7 ? 'checked' : '';
echo ' />' . "\r\n" . '                                                    <label for="theme-api" data-on-label="Yes" data-off-label="No"></label>' . "\r\n" . '                                                </div>' . "\r\n" . '                                            </div>' . "\r\n\r\n" . '                                        </div>' . "\r\n" . '                                        <div class="form-group">' . "\r\n" . '                                            <button class="btn btn-primary" name="submit_theme" type="submit">Save</button>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </form>' . "\r\n" . '                                </div>' . "\r\n\r\n" . '                                <div id="accordion" class="custom-accordion">' . "\r\n" . '                                    <div class="card mb-1 shadow-none">' . "\r\n" . '                                        <a href="#collapseOne" class="text-dark" data-toggle="collapse" aria-expanded="false" aria-controls="collapseOne">' . "\r\n" . '                                            <div class="card-header" id="headingOne">' . "\r\n" . '                                                <h6 class="m-0">' . "\r\n" . '                                                    Theme Images' . "\r\n" . '                                                    <i class="mdi mdi-minus float-right accor-plus-icon"></i>' . "\r\n" . '                                                </h6>' . "\r\n" . '                                            </div>' . "\r\n" . '                                        </a>' . "\r\n\r\n" . '                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">' . "\r\n" . '                                            <div class="card-body">' . "\r\n" . '                                                <div class="row">' . "\r\n" . '                                                    
<div class="col-3">' . "\r\n" . '                                                        Default </br>' . "\r\n" . '                                                        <a href="assets/images/layouts/otr-theme_d.png"><img width="265" height="150" src="assets/images/layouts/otr-theme_d.png"></a>' . "\r\n" . '
</div>' . "\r\n\r\n" . '                                                    
<div class="col-3">' . "\r\n" . '                                                        Theme 1 </br>' . "\r\n" . '                                                        <a href="assets/images/layouts/otr-theme_1.png"><img width="265" height="150" src="assets/images/layouts/otr-theme_1.png"></a>' . "\r\n" . '                                                    </div>' . "\r\n\r\n" . '                                                    
<div class="col-3">' . "\r\n" . '                                                        Theme 2 </br>' . "\r\n" . '                                                        <a href="assets/images/layouts/otr-theme_2.png"><img width="265" height="150" src="assets/images/layouts/otr-theme_2.png"></a>' . "\r\n" . '                                                    </div>' . "\r\n\r\n" . '                                                    
<div class="col-3">' . "\r\n" . '                                                        Theme 3 </br>' . "\r\n" . '                                                        <a href="assets/images/layouts/otr-theme_3.png"><img width="265" height="150" src="assets/images/layouts/otr-theme_3.png"></a>' . "\r\n" . '                                                    </div>' . "\r\n" . '                                              </div>' . "\r\n" . '                                            </div>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </div>' . "\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '                    <div  class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <h4 class="card-title">Player Settings</h4>' . "\r\n" . '                                    <p class="card-title-desc">The selected settings will affect all connected applicatons.</br>Not all Apps support Player settings (from panel).</p>' . "\r\n\r\n" . '                                    <div class="col-6 float-left">' . "\r\n" . '                                        ExoPlayer Settings' . "\r\n" . '                                        <div class="card-body">' . "\r\n" . '                                            <form method="POST">' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label class="control-label" for="player_exo_buffer">Set Buffer Size:</label>' . "\r\n" . '                                                    <select class="form-control" id="select" name="player_exo_buffer">' . "\r\n" . '                                                        <option value="10000" ';
echo $xciptv_ort_settings['player_exo_buffer'] == '10000' ? 'selected' : '';
echo '>10 Seconds</option>' . "\r\n" . '                                                        <option value="20000" ';
echo $xciptv_ort_settings['player_exo_buffer'] == '20000' ? 'selected' : '';
echo '>20 Seconds</option>' . "\r\n" . '                                                        <option value="30000" ';
echo $xciptv_ort_settings['player_exo_buffer'] == '30000' ? 'selected' : '';
echo '>30 Seconds</option>' . "\r\n" . '                                                        <option value="40000" ';
echo $xciptv_ort_settings['player_exo_buffer'] == '40000' ? 'selected' : '';
echo '>40 Seconds</option>' . "\r\n" . '                                                        <option value="50000" ';
echo $xciptv_ort_settings['player_exo_buffer'] == '50000' ? 'selected' : '';
echo '>50 Seconds</option>' . "\r\n" . '                                                    </select>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label class="control-label" for="video_resize_exo">Set Zoom:</label>' . "\r\n" . '                                                    <select class="form-control" id="select" name="video_resize_exo">' . "\r\n" . '                                                        <option value="0" ';
echo $xciptv_ort_settings['video_resize_exo'] == '0' ? 'selected' : '';
echo '>Best Fit</option>' . "\r\n" . '                                                        <option value="1" ';
echo $xciptv_ort_settings['video_resize_exo'] == '1' ? 'selected' : '';
echo '>Fixed Height</option>' . "\r\n" . '                                                        <option value="2" ';
echo $xciptv_ort_settings['video_resize_exo'] == '2' ? 'selected' : '';
echo '>Fixed Width</option>' . "\r\n" . '                                                        <option value="3" ';
echo $xciptv_ort_settings['video_resize_exo'] == '3' ? 'selected' : '';
echo '>Fill</option>' . "\r\n" . '                                                        <option value="4" ';
echo $xciptv_ort_settings['video_resize_exo'] == '4' ? 'selected' : '';
echo '>Zoom</option>' . "\r\n" . '                                                    </select>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label class="control-label" for="exo_hw">Enable Hardware Decoder:</label>' . "\r\n" . '                                                    <select class="form-control" id="select" name="exo_hw">' . "\r\n" . '                                                        <option value="yes" ';
echo $xciptv_ort_settings['exo_hw'] == 'yes' ? 'selected' : '';
echo '>Yes</option>' . "\r\n" . '                                                        <option value="no" ';
echo $xciptv_ort_settings['exo_hw'] == 'no' ? 'selected' : '';
echo '>No</option>' . "\r\n" . '                                                    </select>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label class="control-label" for="video_subtitles_exo">Enable Subtitles:</label>' . "\r\n" . '                                                    <select class="form-control" id="select" name="video_subtitles_exo">' . "\r\n" . '                                                        <option value="yes" ';
echo $xciptv_ort_settings['video_subtitles_exo'] == 'yes' ? 'selected' : '';
echo '>Yes</option>' . "\r\n" . '                                                        <option value="no" ';
echo $xciptv_ort_settings['video_subtitles_exo'] == 'no' ? 'selected' : '';
echo '>No</option>' . "\r\n" . '                                                    </select>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label class="control-label" for="last_volume_exo">Set Default Volume:</label>' . "\r\n" . '                                                    <select class="form-control" id="select" name="last_volume_exo">' . "\r\n" . '                                                        <option value="60" ';
echo $xciptv_ort_settings['last_volume_exo'] == '60' ? 'selected' : '';
echo '>60%</option>' . "\r\n" . '                                                        <option value="70" ';
echo $xciptv_ort_settings['last_volume_exo'] == '70' ? 'selected' : '';
echo '>70%</option>' . "\r\n" . '                                                        <option value="80" ';
echo $xciptv_ort_settings['last_volume_exo'] == '80' ? 'selected' : '';
echo '>80%</option>' . "\r\n" . '                                                        <option value="90" ';
echo $xciptv_ort_settings['last_volume_exo'] == '90' ? 'selected' : '';
echo '>90%</option>' . "\r\n" . '                                                        <option value="100" ';
echo $xciptv_ort_settings['last_volume_exo'] == '100' ? 'selected' : '';
echo '>100%</option>' . "\r\n" . '                                                    </select>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <button class="btn btn-primary" name="submit_exo" type="submit">Save</button>' . "\r\n" . '                                                </div>' . "\r\n" . '                                            </form>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </div>' . "\r\n\r\n" . '                                    <div class="col-6 float-right">' . "\r\n" . '                                        VLC-Player Settings' . "\r\n" . '                                        <div class="card-body">' . "\r\n" . '                                            <form method="POST">' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label class="control-label" for="player_vlc_buffer">Set Buffer Size:</label>' . "\r\n" . '                                                    <select class="form-control" id="select" name="player_vlc_buffer">' . "\r\n" . '                                                        <option value="300" ';
echo $xciptv_ort_settings['player_vlc_buffer'] == '300' ? 'selected' : '';
echo '>300 Miliseconds</option>' . "\r\n" . '                                                        <option value="1000" ';
echo $xciptv_ort_settings['player_vlc_buffer'] == '1000' ? 'selected' : '';
echo '>1 Second</option>' . "\r\n" . '                                                        <option value="2000" ';
echo $xciptv_ort_settings['player_vlc_buffer'] == '2000' ? 'selected' : '';
echo '>2 Second</option>' . "\r\n" . '                                                        <option value="3000" ';
echo $xciptv_ort_settings['player_vlc_buffer'] == '3000' ? 'selected' : '';
echo '>3 Second</option>' . "\r\n" . '                                                        <option value="5000" ';
echo $xciptv_ort_settings['player_vlc_buffer'] == '5000' ? 'selected' : '';
echo '>5 Second</option>' . "\r\n" . '                                                    </select>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label class="control-label" for="video_resize_vlc">Set Zoom:</label>' . "\r\n" . '                                                    <select class="form-control" id="select" name="video_resize_vlc">' . "\r\n" . '                                                        <option value="0" ';
echo $xciptv_ort_settings['video_resize_vlc'] == '0' ? 'selected' : '';
echo '>Best Fit</option>' . "\r\n" . '                                                        <option value="1" ';
echo $xciptv_ort_settings['video_resize_vlc'] == '1' ? 'selected' : '';
echo '>16:9</option>' . "\r\n" . '                                                        <option value="2" ';
echo $xciptv_ort_settings['video_resize_vlc'] == '2' ? 'selected' : '';
echo '>4:3</option>' . "\r\n" . '                                                        <option value="3" ';
echo $xciptv_ort_settings['video_resize_vlc'] == '3' ? 'selected' : '';
echo '>Original</option>' . "\r\n" . '                                                        <option value="4" ';
echo $xciptv_ort_settings['video_resize_vlc'] == '4' ? 'selected' : '';
echo '>Fill</option>' . "\r\n" . '                                                    </select>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label class="control-label" for="vlc_hw">Enable Hardware Decoder:</label>' . "\r\n" . '                                                    <select class="form-control" id="select" name="vlc_hw">' . "\r\n" . '                                                        <option value="yes" ';
echo $xciptv_ort_settings['vlc_hw'] == 'yes' ? 'selected' : '';
echo '>Yes</option>' . "\r\n" . '                                                        <option value="no" ';
echo $xciptv_ort_settings['vlc_hw'] == 'no' ? 'selected' : '';
echo '>No</option>' . "\r\n" . '                                                    </select>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label class="control-label" for="video_subtitles_vlc">Enable Subtitles:</label>' . "\r\n" . '                                                    <select class="form-control" id="select" name="video_subtitles_vlc">' . "\r\n" . '                                                        <option value="yes" ';
echo $xciptv_ort_settings['video_subtitles_vlc'] == 'yes' ? 'selected' : '';
echo '>Yes</option>' . "\r\n" . '                                                        <option value="no" ';
echo $xciptv_ort_settings['video_subtitles_vlc'] == 'no' ? 'selected' : '';
echo '>No</option>' . "\r\n" . '                                                    </select>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label class="control-label" for="last_volume_vlc">Set Default Volume:</label>' . "\r\n" . '                                                    <select class="form-control" id="select" name="last_volume_vlc">' . "\r\n" . '                                                        <option value="60" ';
echo $xciptv_ort_settings['last_volume_vlc'] == '60' ? 'selected' : '';
echo '>60%</option>' . "\r\n" . '                                                        <option value="70" ';
echo $xciptv_ort_settings['last_volume_vlc'] == '70' ? 'selected' : '';
echo '>70%</option>' . "\r\n" . '                                                        <option value="80" ';
echo $xciptv_ort_settings['last_volume_vlc'] == '80' ? 'selected' : '';
echo '>80%</option>' . "\r\n" . '                                                        <option value="90" ';
echo $xciptv_ort_settings['last_volume_vlc'] == '90' ? 'selected' : '';
echo '>90%</option>' . "\r\n" . '                                                        <option value="100" ';
echo $xciptv_ort_settings['last_volume_vlc'] == '100' ? 'selected' : '';
echo '>100%</option>' . "\r\n" . '                                                    </select>' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <button class="btn btn-primary" name="submit_vlc" type="submit">Save</button>' . "\r\n" . '                                                </div>' . "\r\n" . '                                            </form>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    </div>' . "\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '


';

include 'assets/includes/footer.php';
echo '    </div>' . "\r\n\r\n" . '    </div>' . "\r\n\r\n" . '    ';
include 'assets/includes/right-sidebar.php';
echo "\r\n" . '    ';
include 'assets/includes/vendor-scripts.php';
echo "\r\n" . '    <script src="./assets/js/app.js"></script>' . "\r\n\r\n" . '</body>' . "\r\n\r\n" . '</html>';

?>