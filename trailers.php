<?php
//db call

$db = new SQLite3('./api/ottrun/trailers.db');
//table name
$table_name = "trailers";
//current file var
$base_file = basename($_SERVER["SCRIPT_NAME"]);

//create if not
$db->exec("CREATE TABLE IF NOT EXISTS {$table_name}(id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, api TEXT, header_n TEXT, border_c TEXT, background_c TEXT, text_c TEXT, days TEXT, auto_s TEXT, url TEXT, urljs TEXT, type TEXT)");

$rows = $db->query("SELECT COUNT(*) as count FROM {$table_name}");
$row = $rows->fetchArray();
$numRows = $row['count'];
if ($numRows == 0)
{
	$db->exec("INSERT INTO {$table_name}(id, api, header_n, border_c, background_c, text_c, days, auto_s, url, urljs, type) VALUES('1', '5cc316f797659', 'Event', '#000000', '#000000', '#ffffff', '3', '1', 'https://google.co.uk','Enter Iframe  Widget', '0')");
}

//update call
@$resU = $db->query("SELECT * FROM {$table_name} WHERE id='1'");
@$rowU=$resU->fetchArray();
$new_url = str_replace(' ', '', $rowU['url']);

$new_js = $rowU['urljs'];

if(isset($_POST['submit'])){
    $new_url = str_replace(' ', '', $_POST['url']);
    $new_js = htmlentities($_POST['urljs']);
	$db->exec("UPDATE {$table_name} SET api='".$_POST['api']."',
	                                    header_n='".$_POST['header_n']."',
										border_c='".$_POST['border_c']."', 
										background_c='".$_POST['background_c']."', 
										text_c='".$_POST['text_c']."',
										days='".$_POST['days']."',
										auto_s='".$_POST['auto_s']."',
										url='".$new_url."',
										urljs='".$new_js."',
										type='".$_POST['type']."'
									WHERE 
										id='1'");
	$db->close();
	header("Location: {$base_file}");
}

?>
<!doctype html>
<html lang="en">

<head>
    <?php include('assets/includes/head-css.php') ?>

</head>

<body data-sidebar="dark">

    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner">
                <i class="ri-loader-line spin-icon"></i>
            </div>
        </div>
    </div>

    <div id="layout-wrapper">

        <?php include('assets/includes/topbar.php') ?>

        <?php include('assets/includes/sidebar.php') ?>

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->

                    <!-- Column -->

     <div class="main-content">

            <div class="page-content">
                
                
                                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="mb-0"> </h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Applications</a></li>
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Video</a></li>
                                        <li class="breadcrumb-item active">Trailer Widget</li>
                                    </ol>
                                </div>

                            </div>
                
                
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="mb-0"> </h4>

            
                        
                        
 
 
 
 
 



 </label>
                     </form>
                </div></center>
                
                
                
                
                
                
                
		<div class="card border-left-primary shadow h-100 card shadow mb-4">
			<div class="">
				<div class="">
					<div class="card-header">
						<center>
						    						    <a href="index.php" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="https://i.imgur.com/QNjgua6.png" alt="" height="42">
                    </span>
                    <span class="logo-lg">
                        <img src="https://i.imgur.com/QNjgua6.png" alt="" height="40">
                    </span>
                </a>
							<h1><i class="icon icon-wrench"></i> Trailers</h1>
						</center>
					</div>
					<div class="card-body">
														<form method="post">
				  
								<div class="form-group ">
									<div class="form-line">

<center>
									  <select hidden="hidden" class="form-control type" id="type" name="type">
										   <option data-value="op2" value="2" <?=$rowU['type']=='2'?'selected':'' ?>>
										  </option>										  
										 
									  </select>
									  </center>
									</div>
								</div>
								<div class="activeu">		  
								<div class="form-group ">
									<div class="form-line">
									  <label class="form-group form-float form-group-lg">trailers URL</label>
									  <input type="text" class="form-control text-primary" style="text-align:center;"
									  name="url" value="<?=$new_url?>" placeholder="Enter trailers URL">
									</div>
								</div>
								</div>
								<center><div class="activej">		  
								<div class="form-group ">
									<div class="form-line">
									  </center><label class="form-group form-float form-group-lg">Iframe Code</label>
									  
									  <textarea  rows="6" id="urls" name="urljs"  class="form-control text-primary" style="text-align:center;"><?php echo $new_js;?></textarea>
									</div>
								</div>
								</div></center>
								
								<div class="actived">
								    
								<div class="form-group ">
									<div class="form-line">
										<label class="form-group form-float form-group-lg">Api Code</label>
										<input class="form-control" name="api" value="<?=$rowU['api']?>" type="text"/>
									</div>
								</div>
								
								<div class="form-group ">
									<div class="form-line">
										<label class="form-group form-float form-group-lg">Border</label>
										<input class="form-control" name="border_c" value="<?=$rowU['border_c']?>" type="color"/>
									</div>
								</div>

								<div class="form-group ">
									<div class="form-line">
										<label class="form-group form-float form-group-lg">Background Color</label>
										<input class="form-control" name="background_c" value="<?=$rowU['background_c']?>" type="color"/>
									</div>
								</div>

								<div class="form-group ">
									<div class="form-line">
										<label class="form-group form-float form-group-lg">Text Color</label>
										<input class="form-control" name="text_c" value="<?=$rowU['text_c']?>" type="color"/>
									</div>
								</div>

								<div class="form-group ">
									<div class="form-line">
									  <label class="form-group form-float form-group-lg">Days</label>
									  <select class="form-control" id="select" name="days">
										  <option value="1" <?=$rowU['days']=='1'?'selected':'' ?>>1</option>
										  <option value="3" <?=$rowU['days']=='3'?'selected':'' ?>>3</option>
										  <option value="7" <?=$rowU['days']=='7'?'selected':'' ?>>7</option>
									  </select>
									</div>
								</div>
				  
								<div class="form-group ">
									<div class="form-line">
									  <label class="form-group form-float form-group-lg">Auto Scroll</label>
									  <select class="form-control" id="select" name="auto_s">
										  <option value="0" <?=$rowU['auto_s']=='0'?'selected':'' ?>>No</option>
										  <option value="1" <?=$rowU['auto_s']=='1'?'selected':'' ?>>Yes</option>
									  </select>
									</div>
								</div>
								<div class="form-group ">
									<div class="form-line">
										<label class="form-group form-float form-group-lg">Header Name</label>
										<input class="form-control" name="header_n" value="<?=$rowU['header_n']?>" type="text"/>
									</div>
								</div>

								
</div>
								<hr>

								<div class="form-group">
									<center>
										<button class="btn btn-primary btn-icon-split" name="submit" type="submit" ><br>
											<i class="icon icon-check">Update Status</i>
										</button>
									</center>
								</div>
							</form>	 
						</div>
					</div>        
        
        
        <div class="col">
            <div class="card border-left-primary shadow h-100 card shadow mb-4">
               <div class="card-header">
						<center>
							<h2><i class="icon icon-wrench"></i> Preview</h2>
						</center>
					</div>
                <iframe id="iframe_preview" style="width: 100%; height: 437px;" src="./api/ottrun/trailer.php"></iframe>
                </div>
                  </div>




				</div>
				                                                           

		</div>
		
		
		<footer class="footer">
    <div class="container-fluid">
        <div class="row">

                        <div class="col-sm-6">
                <script>document.write(new Date().getFullYear())</script>2023 © https://domain/
            </div>
            <div class="col-sm-6">
                <div class="text-sm-right d-none d-sm-block">
                    Crafted with <i class="mdi mdi-heart text-danger"></i> by <a href="https://domain/idname/">Your Name</a> &amp; <a href="https://domain/idname/">Your Name</a>
                </div>
            </div>
                    </div>
    </div>
</footer>
		    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script>
//select activecode form
//var response = {};
//response.val = "op2";
//$("#codemode option[data-value='" + response.val +"']").attr("selected","selected");

//hide activecode form
//$('.actived').show(); 
//$('.activeu').hide(); 
//$('.activej').hide(); 


//Show/hide activecode select
$(document).ready(function(){
  $('.type').change(function(){
    if($('.type').val() == '2') {
      $('.activej').show(); 
      $('.activeu').hide(); 
      $('.actived').hide(); 
    }
    if($('.type').val() == '1') {
      $('.activej').hide(); 
      $('.activeu').show(); 
      $('.actived').hide(); 
    }
    if($('.type').val() == '0') {
      $('.activej').hide(); 
      $('.activeu').hide(); 
      $('.actived').show(); 
     //document.getElementById("activeu").value = ' ';
     //document.getElementById("activej").value = ' ';
    } 
  });
  $('.type').ready(function(){
    if($('.type').val() == '2') {
      $('.activeu').hide(); 
      $('.actived').hide(); 
      $('.activej').show(); 
    }
    if($('.type').val() == '1') {
      $('.activej').hide(); 
      $('.activeu').show(); 
      $('.actived').hide(); 
    }
    if($('.type').val() == '0') {
      $('.activej').hide(); 
      $('.activeu').hide(); 
      $('.actived').show(); 
    // document.getElementById("actived").value = ' ';
      
    } 
  });
});
</script>
<style>
  


.bg-primary {
    background-color: #2e2e4c!important;
}

body {
    margin: 0;
    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: center;
    background-color: #fff;
}



element.style {
}
.card-body {
    -webkit-box-flex: 1;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1.25rem;
}
*, ::after, ::before {
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}
user agent stylesheet
div {
    display: block;
}
.card {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #252b3b;
    background-clip: border-box;
    border: 0 solid #2d3448;
    border-radius: 0.25rem;
}


.input-group {
    position: relative;
    display: flex;
    text-align: center;
    /* align-items: center; */
    width: 90%;
    margin-left: auto;
    margin-right: auto;
}

body {
    margin: 0;
    font-family: Nunito,sans-serif;
    font-size: .9rem;
    font-weight: 400;
    line-height: 1.5;
    color: #79859c;
    text-align: left;
    background-color: #1d222e;
}

.row {
    display: -ms-flexbox;
    display: contents;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}

</style>

                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    </div>

    <?php include('assets/includes/right-sidebar.php'); ?>

    <?php include('assets/includes/vendor-scripts.php'); ?>

    <script src="./assets/js/app.js"></script>

</body>

</html>