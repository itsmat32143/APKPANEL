<?php
/**
*
* @ This file is created by http://DeZender.Net
* @ deZender (PHP7 Decoder for SourceGuardian Encoder)
*
* @ Version			:	4.1.0.1
* @ Author			:	DeZender
* @ Release on		:	29.08.2020
* @ Official site	:	http://DeZender.Net
*
*/

session_start();
include 'assets/includes/db.php';
include 'assets/includes/config.php';

if ($_ERRORS) {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}
if (!isset($_SESSION['loggedin']) && !$_SESSION['loggedin']) {
	header('location: logout.php');
}

if (isset($_POST['post_tvsg'])) {
	$sql = 'UPDATE tvsg_settings SET ';
	$sql .= 'widget_id = \'' . $_POST['widget_id'] . '\', ';
	$sql .= 'border_color = \'' . $_POST['border_color'] . '\', ';
	$sql .= 'background_color = \'' . $_POST['background_color'] . '\', ';
	$sql .= 'text_color = \'' . $_POST['text_color'] . '\',';
	$sql .= 'heading = \'' . $_POST['heading'] . '\', ';
	$sql .= 'auto_scroll = \'' . (isset($_POST['auto_scroll']) ? '1' : '0') . '\';';
	$sqlite3->exec($sql);
	header('Location: webviews_tvsportsguide.php');
}

echo '<!doctype html>' . "\r\n" . '<html lang="en">' . "\r\n\r\n" . '<head>' . "\r\n\r\n" . '    ';
include 'assets/includes/title-meta.php';
echo "\r\n" . '    ';
include 'assets/includes/head-css.php';
echo "\r\n" . '</head>' . "\r\n\r\n" . '<body data-sidebar="dark">' . "\r\n\r\n" . '    <!-- Loader -->' . "\r\n" . '    <div id="preloader">' . "\r\n" . '        <div id="status">' . "\r\n" . '            <div class="spinner">' . "\r\n" . '                <i class="ri-loader-line spin-icon"></i>' . "\r\n" . '            </div>' . "\r\n" . '        </div>' . "\r\n" . '    </div>' . "\r\n\r\n" . '    <div id="layout-wrapper">' . "\r\n\r\n" . '        ';
include 'assets/includes/topbar.php';
echo "\r\n" . '        ';
include 'assets/includes/sidebar.php';
echo "\r\n" . '        <div class="main-content">' . "\r\n\r\n" . '            <div class="page-content">' . "\r\n" . '                <div class="container-fluid">' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="page-title-box d-flex align-items-center justify-content-between">' . "\r\n" . '                                <h4 class="mb-0"> </h4>' . "\r\n\r\n" . '                                <div class="page-title-right">' . "\r\n" . '                                    <ol class="breadcrumb m-0">' . "\r\n" . '                                        <li class="breadcrumb-item"><a href="javascript: void(0);">WebViews</a></li>' . "\r\n" . '                                        <li class="breadcrumb-item active">TV Sports Guide</li>' . "\r\n" . '                                    </ol>' . "\r\n" . '                                </div>' . "\r\n\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <div class="col-12 d-flex mx-auto">' . "\r\n\r\n" . '                                        <div class="card-body">' . "\r\n" . '                                            <div class="col-md-12">' . "\r\n\r\n" . '                                                <div class="row">' . "\r\n" . '                                                    <div class="col-12 col-md-6">' . "\r\n" . '                                                        <div class="card">' . "\r\n" . '                                                            <div class="card-body">' . "\r\n" . '                                                                <h5 class="card-title">TV Sports Guide</h5>' . "\r\n" . '                                                                <hr class="mt-0" />' . "\r\n" . '                                                                <form method="POST" id="post_tvsg">' . "\r\n" . '                                                                    <div class="form-row">' . "\r\n" . '                                                                        <div class="form-group col-12 col-md-6">' . "\r\n" . '                                                                            <div class="d-flex-between">' . "\r\n" . '                                                                                <label class="mb-0">Auto-scroll</label>' . "\r\n" . '                                                                                <div class="square-switch float-right">' . "\r\n" . '                                                                                    <input type="checkbox" name="auto_scroll" id="auto_scroll" switch="bool" ';
echo $tvsg_settings['auto_scroll'] == '1' ? 'checked' : '';
echo ' />' . "\r\n" . '                                                                                    <label for="auto_scroll" data-on-label="Yes" data-off-label="No"></label>' . "\r\n" . '                                                                                </div>' . "\r\n" . '                                                                            </div>' . "\r\n" . '                                                                        </div>' . "\r\n" . '                                                                        <div class="col-12">' . "\r\n" . '                                                                            <hr class="mt-0" />' . "\r\n" . '                                                                        </div>' . "\r\n" . '                                                                        <div class="form-group col-12 col-md-6">' . "\r\n" . '                                                                            <label>Border Color</label>' . "\r\n" . '                                                                            <input type="color" name="border_color" id="border_color" class="form-control" value="';
echo $tvsg_settings['border_color'];
echo '" />' . "\r\n" . '                                                                        </div>' . "\r\n" . '                                                                        <div class="form-group col-12 col-md-6">' . "\r\n" . '                                                                            <label>Background Color</label>' . "\r\n" . '                                                                            <input type="color" name="background_color" id="background_color" class="form-control" value="';
echo $tvsg_settings['background_color'];
echo '" />' . "\r\n" . '                                                                        </div>' . "\r\n" . '                                                                        <div class="form-group col-12 col-md-6">' . "\r\n" . '                                                                            <label>Font Color</label>' . "\r\n" . '                                                                            <input type="color" name="text_color" id="text_color" class="form-control" value="';
echo $tvsg_settings['text_color'];
echo '" />' . "\r\n" . '                                                                        </div>' . "\r\n" . '                                                                        <div class="col-12">' . "\r\n" . '                                                                            <hr class="mt-0" />' . "\r\n" . '                                                                        </div>' . "\r\n" . '                                                                        <div class="form-group col-12 col-md-6">' . "\r\n" . '                                                                            <label class="mb-0">Widget ID <a href="/assets/images/tvspg.png"><small><i class="ri-question-line"></i></small></a></label>' . "\r\n" . '                                                                            <input type="text" class="form-control float-right" id="widget_id" name="widget_id" placeholder="Widget ID" value="';
echo $tvsg_settings['widget_id'];
echo '">' . "\r\n" . '                                                                        </div>' . "\r\n" . '                                                                        <div class="form-group col-12 col-md-6">' . "\r\n" . '                                                                            <div class="d-flex-between">' . "\r\n" . '                                                                                <label class="mb-0">Heading</label>' . "\r\n" . '                                                                                <input type="text" class="form-control float-right" id="heading" name="heading" placeholder="Heading" value="';
echo $tvsg_settings['heading'];
echo '">' . "\r\n" . '                                                                            </div>' . "\r\n" . '                                                                        </div>' . "\r\n" . '                                                                    </div>' . "\r\n" . '                                                                    <hr class="mt-0" />' . "\r\n" . '                                                                    <button class="btn btn-primary" name="post_tvsg" type="submit">Update</button>' . "\r\n" . '                                                                </form>' . "\r\n" . '                                                            </div>' . "\r\n" . '                                                        </div>' . "\r\n" . '                                                    </div>' . "\r\n" . '                                                    <div class="col-12 col-md-6">' . "\r\n" . '                                                        <div class="card">' . "\r\n" . '                                                            <div class="card-body">' . "\r\n" . '                                                                <h5 class="card-title">Preview</h5>' . "\r\n" . '                                                                <hr class="mt-0" />' . "\r\n\r\n" . '                                                                <iframe id="iframe_preview" style="width: 100%; height: 382px;" src="https://www.tvsportguide.com/widget/';
echo $tvsg_settings['widget_id'];
echo '/?heading=';
echo $tvsg_settings['heading'];
echo '&border_color=custom&autoscroll=';
echo $tvsg_settings['auto_scroll'];
echo '&custom_colors=';
echo substr($tvsg_settings['border_color'], 1);
echo ',';
echo substr($tvsg_settings['background_color'], 1);
echo ',';
echo substr($tvsg_settings['text_color'], 1);
echo '">' . "\r\n" . '                                                                </iframe>' . "\r\n\r\n" . '                                                            </div>' . "\r\n" . '                                                        </div>' . "\r\n" . '                                                    </div>' . "\r\n" . '                                                    <div class="col-12">' . "\r\n" . '                                                        <div class="card">' . "\r\n" . '                                                            <div class="card-body">' . "\r\n" . '                                                                <h5 class="card-title">Endpoint</h5>' . "\r\n" . '                                                                <hr class="mt-0" />' . "\r\n\r\n" . '                                                                <p>You can add to your custom WebView using this URL: <a href="';
echo str_replace('\\', '', (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] !== 'off') ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/api/webview/tvsg.php');
echo '">';
echo str_replace('\\', '', (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] !== 'off') ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/api/webview/tvsg.php');
echo '</a>' . "\r\n" . '                                                                ' . "\r\n" . '                                                            </div>' . "\r\n" . '                                                        </div>' . "\r\n" . '                                                    </div>' . "\r\n" . '                                                </div>' . "\r\n\r\n" . '                                            </div>' . "\r\n\r\n" . '                                        </div>' . "\r\n" . '                                    </div>' . "\r\n\r\n" . '                                </div>' . "\r\n\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n" . '                </div>' . "\r\n" . '            </div>' . "\r\n" . '        </div>' . "\r\n\r\n" . '        ';
include 'assets/includes/footer.php';
echo '    </div>' . "\r\n\r\n" . '    </div>' . "\r\n\r\n" . '    ';
include 'assets/includes/right-sidebar.php';
echo "\r\n" . '    ';
include 'assets/includes/vendor-scripts.php';
echo "\r\n" . '    <script src="./assets/js/app.js"></script>' . "\r\n\r\n" . '</body>' . "\r\n\r\n" . '</html>';

?>