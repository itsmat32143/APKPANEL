<?php


session_start();
include 'assets/includes/db.php';
include 'assets/includes/config.php';

if ($_ERRORS) {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}
if (!isset($_SESSION['loggedin']) && !$_SESSION['loggedin']) {
	header('location: logout.php');
}

if (isset($_POST['update_submit'])) {
	$sql = 'UPDATE xc_xciptv_lic SET ';
	$sql .= 'cid = \'' . $_POST['cid'] . '\', ';
	$sql .= 'appname = \'' . $_POST['appname'] . '\';';
	$sqlite3->exec($sql);
	header('Location: ottrunxciptv_licset.php');
}

echo '<!doctype html>' . "\r\n" . '<html lang="en">' . "\r\n\r\n" . '<head>' . "\r\n\r\n" . '    ';
include 'assets/includes/title-meta.php';
echo "\r\n" . '    ';
include 'assets/includes/head-css.php';
echo "\r\n" . '</head>' . "\r\n\r\n" . '<body data-sidebar="dark">' . "\r\n\r\n" . '    <!-- Loader -->' . "\r\n" . '    <div id="preloader">' . "\r\n" . '        <div id="status">' . "\r\n" . '            <div class="spinner">' . "\r\n" . '                <i class="ri-loader-line spin-icon"></i>' . "\r\n" . '            </div>' . "\r\n" . '        </div>' . "\r\n" . '    </div>' . "\r\n\r\n" . '    <div id="layout-wrapper">' . "\r\n\r\n" . '        ';
include 'assets/includes/topbar.php';
echo "\r\n" . '        ';
include 'assets/includes/sidebar.php';
echo "\r\n" . '        <div class="main-content">' . "\r\n\r\n" . '            <div class="page-content">' . "\r\n" . '                <div class="container-fluid">' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="page-title-box d-flex align-items-center justify-content-between">' . "\r\n" . '                                <h4 class="mb-0"> </h4>' . "\r\n\r\n" . '                                <div class="page-title-right">' . "\r\n" . '                                    <ol class="breadcrumb m-0">' . "\r\n" . '                                        <li class="breadcrumb-item"><a href="javascript: void(0);">OTTRun XCIPTV</a></li>' . "\r\n" . '                                        <li class="breadcrumb-item active">Licence</li>' . "\r\n" . '                                    </ol>' . "\r\n" . '                                </div>' . "\r\n\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '                    <div class="row">' . "\r\n" . '                        <div class="col-12">' . "\r\n" . '                            <div class="card">' . "\r\n" . '                                <div class="card-body">' . "\r\n\r\n" . '                                    <h4 class="card-title">Licence Information</h4>' . "\r\n" . '                                    <p class="card-title-desc">Update your application Licence information.</p>' . "\r\n\r\n" . '                                    <div class="alert alert-danger alert-dismissible fade show  d-flex mx-auto col-4" role="alert">' . "\r\n" . '                                        <center>' . "\r\n" . '                                            Any application connected to this panel that is using the Licence v4 system (>724) please note for the application to work: <br>The panel <strong><u>must</u></strong> match the application.' . "\r\n" . '                                        </center>' . "\r\n" . '                                    </div>' . "\r\n\r\n" . '                                    <br />' . "\r\n\r\n" . '                                    <div class="col-4 d-flex mx-auto">' . "\r\n\r\n" . '                                        <div class="card-body">' . "\r\n" . '                                            <form method="POST">' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label for="cid">Customer ID</label>' . "\r\n" . '                                                    <input class="form-control" id="cid" name="cid" value="';
echo $xciptv_lic_data['cid'];
echo '" readonly="text" />' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <label for="appname">App Name</label>' . "\r\n" . '                                                    <input class="form-control" id="appname" name="appname" value="';
echo $xciptv_lic_data['appname'];
echo '" type="text" />' . "\r\n" . '                                                </div>' . "\r\n" . '                                                <div class="form-group">' . "\r\n" . '                                                    <button class="btn btn-primary" name="update_submit" type="update_submit">Save</button>' . "\r\n" . '                                                </div>' . "\r\n" . '                                            </form>' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                    </div>' . "\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n\r\n" . '                </div>' . "\r\n" . '            </div>' . "\r\n\r\n" . '            ';
include 'assets/includes/footer.php';
echo '        </div>' . "\r\n\r\n" . '    </div>' . "\r\n\r\n" . '    ';
include 'assets/includes/right-sidebar.php';
echo "\r\n" . '    ';
include 'assets/includes/vendor-scripts.php';
echo "\r\n" . '    <script src="./assets/js/app.js"></script>' . "\r\n\r\n" . '</body>' . "\r\n\r\n" . '</html>';

?>