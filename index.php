<?php



session_start();
include 'assets/includes/db.php';
include 'assets/includes/config.php';

if ($_ERRORS) {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']) {
	header('location: dashboard.php');
}

if (isset($_POST['login'])) {
	$sql = 'SELECT * FROM profile ';
	$sql .= 'WHERE username = "' . $_POST['username'] . '";';
	$result = $sqlite3->query($sql);

	while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
		$id = $row['id'];
		$username = $row['username'];
		$password = $row['password'];
		$profile_name = $row['profile_name'];
		$avatar_url = $row['avatar_url'];
	}

	if (isset($id)) {
		if ($password == $_POST['password']) {
			$_SESSION['loggedin'] = true;
			$_SESSION['username'] = $_POST['username'];
			$_SESSION['profile_name'] = $_POST['profile_name'];
			$_SESSION['avatar_url'] = $_POST['avatar_url'];
			if (($_POST['username'] == 'admin') && ($_POST['password'] == 'admin')) {
				$_SESSION['loggedin'] = true;
				header('location: profile_edit.php');
			}
			else {
				$_SESSION['loggedin'] = true;

				if ($DASHBOARD) {
					header('location: dashboard.php');
				}
				else {
					header('location: profile_edit.php');
				}
			}
		}
		else {
			header('location: ./api/index.php');
		}
	}
	else {
		header('location: ./api/index.php');
	}
}

echo '<!doctype html>' . "\r\n" . '<html lang="en">' . "\r\n\r\n" . '<head>' . "\r\n\r\n" . '    ';
include './assets/includes/title-meta.php';
echo "\r\n" . '    ';
include './assets/includes/head-css.php';
echo "\r\n" . '</head>' . "\r\n\r\n" . '<body class="auth-body-bg">' . "\r\n" . '    <div>' . "\r\n" . '        <div class="container-fluid p-0">' . "\r\n" . '            <div class="row no-gutters">' . "\r\n" . '                <div class="col-lg-4">' . "\r\n" . '                    <div class="authentication-page-content p-4 d-flex align-items-center min-vh-100">' . "\r\n" . '                        <div class="w-100">' . "\r\n" . '                            <div class="row justify-content-center">' . "\r\n" . '                                <div class="col-lg-9">' . "\r\n" . '                                    <form method="POST">' . "\r\n" . '                                        <div class="text-center">' . "\r\n" . '                                            <div>' . "\r\n" . '    

<img src="https://i.imgur.com/e1e1Z1d.png" alt="" width="100" height="100">

</div>' . "\r\n\r\n" . '                                            <h4 class="font-size-18 mt-4">The Don XC V6 804</h4>' . "\r\n" . '                                            <p class="text-muted">Sign in to continue.</p>' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        <div class="p-2 mt-5">' . "\r\n" . '                                            <form class="form-horizontal" action="index.php">' . "\r\n\r\n" . '                                                <div class="form-group auth-form-group-custom mb-4">' . "\r\n" . '                                                    <i class="ri-user-2-line auti-custom-input-icon"></i>' . "\r\n" . '                                                    <label for="username">Username</label>' . "\r\n" . '                                                    <input type="text" class="form-control" id="username" name="username" placeholder="Enter username">' . "\r\n" . '                                                </div>' . "\r\n\r\n" . '                                                <div class="form-group auth-form-group-custom mb-4">' . "\r\n" . '                                                    <i class="ri-lock-2-line auti-custom-input-icon"></i>' . "\r\n" . '                                                    <label for="userpassword">Password</label>' . "\r\n" . '                                                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">' . "\r\n" . '                                                </div>' . "\r\n\r\n" . '                                                <div class="mt-4 text-center">' . "\r\n" . '                                                    <button class="btn btn-primary w-md waves-effect waves-light" type="submit" name="login">Log In</button>' . "\r\n" . '                                                </div>' . "\r\n" . '                                            </form>' . "\r\n" . '                                        </div>' . "\r\n\r\n" . '                                        ';

if ($MasterAPP_BRANDING) {
	echo '                                        <div class="mt-5 text-center">' . "\r\n" . '                                            <p>' . "\r\n" . '                                                <script>' . "\r\n" . '                                                    document.write(new Date().getFullYear())' . "\r\n" . '                                                </script> © @TheDonCrimini' . "\r\n" . '                                            <p></P>' . "\r\n" . '                                            Crafted with <i class="mdi mdi-heart text-danger"></i> by <a href="https://t.me/TheDonCrimini">The Don</a> ' . "\r\n" . '                                            </p>' . "\r\n" . '                                        </div>' . "\r\n" . '                                    ';
}

echo '                                    </form>' . "\r\n\r\n" . '                                </div>' . "\r\n" . '                            </div>' . "\r\n" . '                        </div>' . "\r\n" . '                    </div>' . "\r\n" . '                </div>' . "\r\n" . '                <div class="col-lg-8">' . "\r\n" . '                    <div class="authentication-bg">' . "\r\n" . '                        <div class="bg-overlay"></div>' . "\r\n" . '                    </div>' . "\r\n" . '                </div>' . "\r\n" . '            </div>' . "\r\n" . '        </div>' . "\r\n" . '    </div>' . "\r\n\r\n" . '    ';
include 'assets/includes/vendor-scripts.php';
echo "\r\n" . '    <script src="assets/js/app.js"></script>' . "\r\n" . '    <style>' . "\r\n" . ' .auth-body-bg {
    background-color: #313131;
}       .authentication-bg {' . "\r\n" . '            background-image: url(';
echo $USER_PROFILE_PANEL_EDITS ? $panel_data['login_gif'] : $USER_PROFILE_PANEL_LOGIN_GIF;
echo ');' . "\r\n" . '            height: 100vh;' . "\r\n" . '            background-size: cover;' . "\r\n" . '            background-position: center;' . "\r\n" . '        }' . "\r\n" . '    </style>' . "\r\n\r\n" . '</body>' . "\r\n\r\n" . '</html>';

?>
